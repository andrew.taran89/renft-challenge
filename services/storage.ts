const getKey = (key: string) => `renftapp::${key}`;

interface Storage {
  saveRecord: <T>(key: string, value: T) => void;
  getRecord: <T>(key: string) => T;
}

export class LocalStorage implements Storage {
  saveRecord<T>(key: string, value: T) {
    const jsonString = JSON.stringify(value);

    return window.localStorage.setItem(getKey(key), jsonString);
  }

  getRecord<T>(key: string): T {
    const jsonString = window.localStorage.getItem(getKey(key));

    return jsonString ? JSON.parse(jsonString) : {};
  }
}

export const storage = new LocalStorage();
