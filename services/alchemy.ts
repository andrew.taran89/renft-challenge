import { Network, Alchemy, NftTokenType } from "alchemy-sdk";
import config from "config";

const settings = {
  apiKey: config.ALCHEMY_API_KEY,
  network: Network.ETH_MAINNET,
};

export const alchemy = new Alchemy(settings);

export const getNftMetadata = (
  nftAddress: string,
  tokenId: string,
  isERC721?: boolean
) => {
  return alchemy.nft.getNftMetadata(
    nftAddress,
    tokenId,
    isERC721 ? NftTokenType.ERC721 : NftTokenType.ERC1155
  );
};
