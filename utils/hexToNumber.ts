import * as web3Utils from "web3-utils";

const hexToNumber = (value: string) => web3Utils.hexToNumber(value);

export default hexToNumber;
