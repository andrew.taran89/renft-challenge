import { FC } from "react";
import { NftCardObserver } from "components/NftCard";
import { LendingsQueryDocument } from ".graphclient";
import { useFavoritesContext } from "contexts/FavoritesContext";
import useQuery from "hooks/useQuery";

const Gallery: FC = () => {
  const { favorites, toggleFavorite } = useFavoritesContext();
  const { data = { lendings: [] } } = useQuery(LendingsQueryDocument, {
    first: 999,
  });

  return (
    <div className="grid gap-4 p-4 justify-center grid-cols-[repeat(auto-fit,minmax(250px,300px))]">
      {data.lendings.map((x) => (
        <NftCardObserver
          key={x.id}
          lending={x}
          favorite={favorites?.[x.id]}
          toggleFavorite={toggleFavorite}
        />
      ))}
    </div>
  );
};

export default Gallery;
