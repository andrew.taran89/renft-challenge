// @ts-nocheck

import { InContextSdkMethod } from '@graphql-mesh/types';
import { MeshContext } from '@graphql-mesh/runtime';

export namespace RenftTypes {
  export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  BigDecimal: any;
  BigInt: any;
  Bytes: any;
};

export type BlockChangedFilter = {
  number_gte: Scalars['Int'];
};

export type Block_height = {
  hash?: InputMaybe<Scalars['Bytes']>;
  number?: InputMaybe<Scalars['Int']>;
  number_gte?: InputMaybe<Scalars['Int']>;
};

export type Counter = {
  id: Scalars['ID'];
  lending: Scalars['Int'];
  renting: Scalars['Int'];
  user: Scalars['Int'];
};

export type Counter_filter = {
  id?: InputMaybe<Scalars['ID']>;
  id_not?: InputMaybe<Scalars['ID']>;
  id_gt?: InputMaybe<Scalars['ID']>;
  id_lt?: InputMaybe<Scalars['ID']>;
  id_gte?: InputMaybe<Scalars['ID']>;
  id_lte?: InputMaybe<Scalars['ID']>;
  id_in?: InputMaybe<Array<Scalars['ID']>>;
  id_not_in?: InputMaybe<Array<Scalars['ID']>>;
  lending?: InputMaybe<Scalars['Int']>;
  lending_not?: InputMaybe<Scalars['Int']>;
  lending_gt?: InputMaybe<Scalars['Int']>;
  lending_lt?: InputMaybe<Scalars['Int']>;
  lending_gte?: InputMaybe<Scalars['Int']>;
  lending_lte?: InputMaybe<Scalars['Int']>;
  lending_in?: InputMaybe<Array<Scalars['Int']>>;
  lending_not_in?: InputMaybe<Array<Scalars['Int']>>;
  renting?: InputMaybe<Scalars['Int']>;
  renting_not?: InputMaybe<Scalars['Int']>;
  renting_gt?: InputMaybe<Scalars['Int']>;
  renting_lt?: InputMaybe<Scalars['Int']>;
  renting_gte?: InputMaybe<Scalars['Int']>;
  renting_lte?: InputMaybe<Scalars['Int']>;
  renting_in?: InputMaybe<Array<Scalars['Int']>>;
  renting_not_in?: InputMaybe<Array<Scalars['Int']>>;
  user?: InputMaybe<Scalars['Int']>;
  user_not?: InputMaybe<Scalars['Int']>;
  user_gt?: InputMaybe<Scalars['Int']>;
  user_lt?: InputMaybe<Scalars['Int']>;
  user_gte?: InputMaybe<Scalars['Int']>;
  user_lte?: InputMaybe<Scalars['Int']>;
  user_in?: InputMaybe<Array<Scalars['Int']>>;
  user_not_in?: InputMaybe<Array<Scalars['Int']>>;
  /** Filter for the block changed event. */
  _change_block?: InputMaybe<BlockChangedFilter>;
};

export type Counter_orderBy =
  | 'id'
  | 'lending'
  | 'renting'
  | 'user';

export type Lending = {
  id: Scalars['ID'];
  cursor: Scalars['Int'];
  nftAddress: Scalars['Bytes'];
  tokenId: Scalars['BigInt'];
  lenderAddress: Scalars['Bytes'];
  maxRentDuration: Scalars['BigInt'];
  dailyRentPrice: Scalars['Bytes'];
  nftPrice: Scalars['Bytes'];
  paymentToken: Scalars['BigInt'];
  lentAmount: Scalars['BigInt'];
  isERC721: Scalars['Boolean'];
  lentAt: Scalars['BigInt'];
  renting?: Maybe<Renting>;
  collateralClaimed: Scalars['Boolean'];
  /** id := nftAddresss::tokenId::lentAmount */
  nft: Nft;
  lenderUser: User;
};

export type LendingRentingCount = {
  id: Scalars['ID'];
  lending: Scalars['BigInt'];
  /** less or equal lending */
  renting: Scalars['BigInt'];
};

export type LendingRentingCount_filter = {
  id?: InputMaybe<Scalars['ID']>;
  id_not?: InputMaybe<Scalars['ID']>;
  id_gt?: InputMaybe<Scalars['ID']>;
  id_lt?: InputMaybe<Scalars['ID']>;
  id_gte?: InputMaybe<Scalars['ID']>;
  id_lte?: InputMaybe<Scalars['ID']>;
  id_in?: InputMaybe<Array<Scalars['ID']>>;
  id_not_in?: InputMaybe<Array<Scalars['ID']>>;
  lending?: InputMaybe<Scalars['BigInt']>;
  lending_not?: InputMaybe<Scalars['BigInt']>;
  lending_gt?: InputMaybe<Scalars['BigInt']>;
  lending_lt?: InputMaybe<Scalars['BigInt']>;
  lending_gte?: InputMaybe<Scalars['BigInt']>;
  lending_lte?: InputMaybe<Scalars['BigInt']>;
  lending_in?: InputMaybe<Array<Scalars['BigInt']>>;
  lending_not_in?: InputMaybe<Array<Scalars['BigInt']>>;
  renting?: InputMaybe<Scalars['BigInt']>;
  renting_not?: InputMaybe<Scalars['BigInt']>;
  renting_gt?: InputMaybe<Scalars['BigInt']>;
  renting_lt?: InputMaybe<Scalars['BigInt']>;
  renting_gte?: InputMaybe<Scalars['BigInt']>;
  renting_lte?: InputMaybe<Scalars['BigInt']>;
  renting_in?: InputMaybe<Array<Scalars['BigInt']>>;
  renting_not_in?: InputMaybe<Array<Scalars['BigInt']>>;
  /** Filter for the block changed event. */
  _change_block?: InputMaybe<BlockChangedFilter>;
};

export type LendingRentingCount_orderBy =
  | 'id'
  | 'lending'
  | 'renting';

export type Lending_filter = {
  id?: InputMaybe<Scalars['ID']>;
  id_not?: InputMaybe<Scalars['ID']>;
  id_gt?: InputMaybe<Scalars['ID']>;
  id_lt?: InputMaybe<Scalars['ID']>;
  id_gte?: InputMaybe<Scalars['ID']>;
  id_lte?: InputMaybe<Scalars['ID']>;
  id_in?: InputMaybe<Array<Scalars['ID']>>;
  id_not_in?: InputMaybe<Array<Scalars['ID']>>;
  cursor?: InputMaybe<Scalars['Int']>;
  cursor_not?: InputMaybe<Scalars['Int']>;
  cursor_gt?: InputMaybe<Scalars['Int']>;
  cursor_lt?: InputMaybe<Scalars['Int']>;
  cursor_gte?: InputMaybe<Scalars['Int']>;
  cursor_lte?: InputMaybe<Scalars['Int']>;
  cursor_in?: InputMaybe<Array<Scalars['Int']>>;
  cursor_not_in?: InputMaybe<Array<Scalars['Int']>>;
  nftAddress?: InputMaybe<Scalars['Bytes']>;
  nftAddress_not?: InputMaybe<Scalars['Bytes']>;
  nftAddress_in?: InputMaybe<Array<Scalars['Bytes']>>;
  nftAddress_not_in?: InputMaybe<Array<Scalars['Bytes']>>;
  nftAddress_contains?: InputMaybe<Scalars['Bytes']>;
  nftAddress_not_contains?: InputMaybe<Scalars['Bytes']>;
  tokenId?: InputMaybe<Scalars['BigInt']>;
  tokenId_not?: InputMaybe<Scalars['BigInt']>;
  tokenId_gt?: InputMaybe<Scalars['BigInt']>;
  tokenId_lt?: InputMaybe<Scalars['BigInt']>;
  tokenId_gte?: InputMaybe<Scalars['BigInt']>;
  tokenId_lte?: InputMaybe<Scalars['BigInt']>;
  tokenId_in?: InputMaybe<Array<Scalars['BigInt']>>;
  tokenId_not_in?: InputMaybe<Array<Scalars['BigInt']>>;
  lenderAddress?: InputMaybe<Scalars['Bytes']>;
  lenderAddress_not?: InputMaybe<Scalars['Bytes']>;
  lenderAddress_in?: InputMaybe<Array<Scalars['Bytes']>>;
  lenderAddress_not_in?: InputMaybe<Array<Scalars['Bytes']>>;
  lenderAddress_contains?: InputMaybe<Scalars['Bytes']>;
  lenderAddress_not_contains?: InputMaybe<Scalars['Bytes']>;
  maxRentDuration?: InputMaybe<Scalars['BigInt']>;
  maxRentDuration_not?: InputMaybe<Scalars['BigInt']>;
  maxRentDuration_gt?: InputMaybe<Scalars['BigInt']>;
  maxRentDuration_lt?: InputMaybe<Scalars['BigInt']>;
  maxRentDuration_gte?: InputMaybe<Scalars['BigInt']>;
  maxRentDuration_lte?: InputMaybe<Scalars['BigInt']>;
  maxRentDuration_in?: InputMaybe<Array<Scalars['BigInt']>>;
  maxRentDuration_not_in?: InputMaybe<Array<Scalars['BigInt']>>;
  dailyRentPrice?: InputMaybe<Scalars['Bytes']>;
  dailyRentPrice_not?: InputMaybe<Scalars['Bytes']>;
  dailyRentPrice_in?: InputMaybe<Array<Scalars['Bytes']>>;
  dailyRentPrice_not_in?: InputMaybe<Array<Scalars['Bytes']>>;
  dailyRentPrice_contains?: InputMaybe<Scalars['Bytes']>;
  dailyRentPrice_not_contains?: InputMaybe<Scalars['Bytes']>;
  nftPrice?: InputMaybe<Scalars['Bytes']>;
  nftPrice_not?: InputMaybe<Scalars['Bytes']>;
  nftPrice_in?: InputMaybe<Array<Scalars['Bytes']>>;
  nftPrice_not_in?: InputMaybe<Array<Scalars['Bytes']>>;
  nftPrice_contains?: InputMaybe<Scalars['Bytes']>;
  nftPrice_not_contains?: InputMaybe<Scalars['Bytes']>;
  paymentToken?: InputMaybe<Scalars['BigInt']>;
  paymentToken_not?: InputMaybe<Scalars['BigInt']>;
  paymentToken_gt?: InputMaybe<Scalars['BigInt']>;
  paymentToken_lt?: InputMaybe<Scalars['BigInt']>;
  paymentToken_gte?: InputMaybe<Scalars['BigInt']>;
  paymentToken_lte?: InputMaybe<Scalars['BigInt']>;
  paymentToken_in?: InputMaybe<Array<Scalars['BigInt']>>;
  paymentToken_not_in?: InputMaybe<Array<Scalars['BigInt']>>;
  lentAmount?: InputMaybe<Scalars['BigInt']>;
  lentAmount_not?: InputMaybe<Scalars['BigInt']>;
  lentAmount_gt?: InputMaybe<Scalars['BigInt']>;
  lentAmount_lt?: InputMaybe<Scalars['BigInt']>;
  lentAmount_gte?: InputMaybe<Scalars['BigInt']>;
  lentAmount_lte?: InputMaybe<Scalars['BigInt']>;
  lentAmount_in?: InputMaybe<Array<Scalars['BigInt']>>;
  lentAmount_not_in?: InputMaybe<Array<Scalars['BigInt']>>;
  isERC721?: InputMaybe<Scalars['Boolean']>;
  isERC721_not?: InputMaybe<Scalars['Boolean']>;
  isERC721_in?: InputMaybe<Array<Scalars['Boolean']>>;
  isERC721_not_in?: InputMaybe<Array<Scalars['Boolean']>>;
  lentAt?: InputMaybe<Scalars['BigInt']>;
  lentAt_not?: InputMaybe<Scalars['BigInt']>;
  lentAt_gt?: InputMaybe<Scalars['BigInt']>;
  lentAt_lt?: InputMaybe<Scalars['BigInt']>;
  lentAt_gte?: InputMaybe<Scalars['BigInt']>;
  lentAt_lte?: InputMaybe<Scalars['BigInt']>;
  lentAt_in?: InputMaybe<Array<Scalars['BigInt']>>;
  lentAt_not_in?: InputMaybe<Array<Scalars['BigInt']>>;
  renting?: InputMaybe<Scalars['String']>;
  renting_not?: InputMaybe<Scalars['String']>;
  renting_gt?: InputMaybe<Scalars['String']>;
  renting_lt?: InputMaybe<Scalars['String']>;
  renting_gte?: InputMaybe<Scalars['String']>;
  renting_lte?: InputMaybe<Scalars['String']>;
  renting_in?: InputMaybe<Array<Scalars['String']>>;
  renting_not_in?: InputMaybe<Array<Scalars['String']>>;
  renting_contains?: InputMaybe<Scalars['String']>;
  renting_contains_nocase?: InputMaybe<Scalars['String']>;
  renting_not_contains?: InputMaybe<Scalars['String']>;
  renting_not_contains_nocase?: InputMaybe<Scalars['String']>;
  renting_starts_with?: InputMaybe<Scalars['String']>;
  renting_starts_with_nocase?: InputMaybe<Scalars['String']>;
  renting_not_starts_with?: InputMaybe<Scalars['String']>;
  renting_not_starts_with_nocase?: InputMaybe<Scalars['String']>;
  renting_ends_with?: InputMaybe<Scalars['String']>;
  renting_ends_with_nocase?: InputMaybe<Scalars['String']>;
  renting_not_ends_with?: InputMaybe<Scalars['String']>;
  renting_not_ends_with_nocase?: InputMaybe<Scalars['String']>;
  renting_?: InputMaybe<Renting_filter>;
  collateralClaimed?: InputMaybe<Scalars['Boolean']>;
  collateralClaimed_not?: InputMaybe<Scalars['Boolean']>;
  collateralClaimed_in?: InputMaybe<Array<Scalars['Boolean']>>;
  collateralClaimed_not_in?: InputMaybe<Array<Scalars['Boolean']>>;
  nft?: InputMaybe<Scalars['String']>;
  nft_not?: InputMaybe<Scalars['String']>;
  nft_gt?: InputMaybe<Scalars['String']>;
  nft_lt?: InputMaybe<Scalars['String']>;
  nft_gte?: InputMaybe<Scalars['String']>;
  nft_lte?: InputMaybe<Scalars['String']>;
  nft_in?: InputMaybe<Array<Scalars['String']>>;
  nft_not_in?: InputMaybe<Array<Scalars['String']>>;
  nft_contains?: InputMaybe<Scalars['String']>;
  nft_contains_nocase?: InputMaybe<Scalars['String']>;
  nft_not_contains?: InputMaybe<Scalars['String']>;
  nft_not_contains_nocase?: InputMaybe<Scalars['String']>;
  nft_starts_with?: InputMaybe<Scalars['String']>;
  nft_starts_with_nocase?: InputMaybe<Scalars['String']>;
  nft_not_starts_with?: InputMaybe<Scalars['String']>;
  nft_not_starts_with_nocase?: InputMaybe<Scalars['String']>;
  nft_ends_with?: InputMaybe<Scalars['String']>;
  nft_ends_with_nocase?: InputMaybe<Scalars['String']>;
  nft_not_ends_with?: InputMaybe<Scalars['String']>;
  nft_not_ends_with_nocase?: InputMaybe<Scalars['String']>;
  nft_?: InputMaybe<Nft_filter>;
  lenderUser?: InputMaybe<Scalars['String']>;
  lenderUser_not?: InputMaybe<Scalars['String']>;
  lenderUser_gt?: InputMaybe<Scalars['String']>;
  lenderUser_lt?: InputMaybe<Scalars['String']>;
  lenderUser_gte?: InputMaybe<Scalars['String']>;
  lenderUser_lte?: InputMaybe<Scalars['String']>;
  lenderUser_in?: InputMaybe<Array<Scalars['String']>>;
  lenderUser_not_in?: InputMaybe<Array<Scalars['String']>>;
  lenderUser_contains?: InputMaybe<Scalars['String']>;
  lenderUser_contains_nocase?: InputMaybe<Scalars['String']>;
  lenderUser_not_contains?: InputMaybe<Scalars['String']>;
  lenderUser_not_contains_nocase?: InputMaybe<Scalars['String']>;
  lenderUser_starts_with?: InputMaybe<Scalars['String']>;
  lenderUser_starts_with_nocase?: InputMaybe<Scalars['String']>;
  lenderUser_not_starts_with?: InputMaybe<Scalars['String']>;
  lenderUser_not_starts_with_nocase?: InputMaybe<Scalars['String']>;
  lenderUser_ends_with?: InputMaybe<Scalars['String']>;
  lenderUser_ends_with_nocase?: InputMaybe<Scalars['String']>;
  lenderUser_not_ends_with?: InputMaybe<Scalars['String']>;
  lenderUser_not_ends_with_nocase?: InputMaybe<Scalars['String']>;
  lenderUser_?: InputMaybe<User_filter>;
  /** Filter for the block changed event. */
  _change_block?: InputMaybe<BlockChangedFilter>;
};

export type Lending_orderBy =
  | 'id'
  | 'cursor'
  | 'nftAddress'
  | 'tokenId'
  | 'lenderAddress'
  | 'maxRentDuration'
  | 'dailyRentPrice'
  | 'nftPrice'
  | 'paymentToken'
  | 'lentAmount'
  | 'isERC721'
  | 'lentAt'
  | 'renting'
  | 'collateralClaimed'
  | 'nft'
  | 'lenderUser';

export type Nft = {
  /** id is nftAddress::tokenId::lentAmount */
  id: Scalars['ID'];
  /** each Lending and Renting in the arrays here will have the SAME nftAddress and tokenId!!!!!! As per the id of this entity */
  lending: Array<Lending>;
  renting?: Maybe<Array<Renting>>;
};


export type NftlendingArgs = {
  skip?: InputMaybe<Scalars['Int']>;
  first?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Lending_orderBy>;
  orderDirection?: InputMaybe<OrderDirection>;
  where?: InputMaybe<Lending_filter>;
};


export type NftrentingArgs = {
  skip?: InputMaybe<Scalars['Int']>;
  first?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Renting_orderBy>;
  orderDirection?: InputMaybe<OrderDirection>;
  where?: InputMaybe<Renting_filter>;
};

export type Nft_filter = {
  id?: InputMaybe<Scalars['ID']>;
  id_not?: InputMaybe<Scalars['ID']>;
  id_gt?: InputMaybe<Scalars['ID']>;
  id_lt?: InputMaybe<Scalars['ID']>;
  id_gte?: InputMaybe<Scalars['ID']>;
  id_lte?: InputMaybe<Scalars['ID']>;
  id_in?: InputMaybe<Array<Scalars['ID']>>;
  id_not_in?: InputMaybe<Array<Scalars['ID']>>;
  lending_?: InputMaybe<Lending_filter>;
  renting_?: InputMaybe<Renting_filter>;
  /** Filter for the block changed event. */
  _change_block?: InputMaybe<BlockChangedFilter>;
};

export type Nft_orderBy =
  | 'id'
  | 'lending'
  | 'renting';

/** Defines the order direction, either ascending or descending */
export type OrderDirection =
  | 'asc'
  | 'desc';

export type Query = {
  lending?: Maybe<Lending>;
  lendings: Array<Lending>;
  renting?: Maybe<Renting>;
  rentings: Array<Renting>;
  nft?: Maybe<Nft>;
  nfts: Array<Nft>;
  user?: Maybe<User>;
  users: Array<User>;
  lendingRentingCount?: Maybe<LendingRentingCount>;
  lendingRentingCounts: Array<LendingRentingCount>;
  counter?: Maybe<Counter>;
  counters: Array<Counter>;
  /** Access to subgraph metadata */
  _meta?: Maybe<_Meta_>;
};


export type QuerylendingArgs = {
  id: Scalars['ID'];
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};


export type QuerylendingsArgs = {
  skip?: InputMaybe<Scalars['Int']>;
  first?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Lending_orderBy>;
  orderDirection?: InputMaybe<OrderDirection>;
  where?: InputMaybe<Lending_filter>;
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};


export type QueryrentingArgs = {
  id: Scalars['ID'];
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};


export type QueryrentingsArgs = {
  skip?: InputMaybe<Scalars['Int']>;
  first?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Renting_orderBy>;
  orderDirection?: InputMaybe<OrderDirection>;
  where?: InputMaybe<Renting_filter>;
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};


export type QuerynftArgs = {
  id: Scalars['ID'];
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};


export type QuerynftsArgs = {
  skip?: InputMaybe<Scalars['Int']>;
  first?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Nft_orderBy>;
  orderDirection?: InputMaybe<OrderDirection>;
  where?: InputMaybe<Nft_filter>;
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};


export type QueryuserArgs = {
  id: Scalars['ID'];
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};


export type QueryusersArgs = {
  skip?: InputMaybe<Scalars['Int']>;
  first?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<User_orderBy>;
  orderDirection?: InputMaybe<OrderDirection>;
  where?: InputMaybe<User_filter>;
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};


export type QuerylendingRentingCountArgs = {
  id: Scalars['ID'];
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};


export type QuerylendingRentingCountsArgs = {
  skip?: InputMaybe<Scalars['Int']>;
  first?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<LendingRentingCount_orderBy>;
  orderDirection?: InputMaybe<OrderDirection>;
  where?: InputMaybe<LendingRentingCount_filter>;
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};


export type QuerycounterArgs = {
  id: Scalars['ID'];
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};


export type QuerycountersArgs = {
  skip?: InputMaybe<Scalars['Int']>;
  first?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Counter_orderBy>;
  orderDirection?: InputMaybe<OrderDirection>;
  where?: InputMaybe<Counter_filter>;
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};


export type Query_metaArgs = {
  block?: InputMaybe<Block_height>;
};

export type Renting = {
  id: Scalars['ID'];
  cursor: Scalars['Int'];
  renterAddress: Scalars['Bytes'];
  rentDuration: Scalars['BigInt'];
  rentedAt: Scalars['BigInt'];
  lending: Lending;
  /** id := nftAddress::tokenId::lentAmount */
  nft: Nft;
  renterUser: User;
};

export type Renting_filter = {
  id?: InputMaybe<Scalars['ID']>;
  id_not?: InputMaybe<Scalars['ID']>;
  id_gt?: InputMaybe<Scalars['ID']>;
  id_lt?: InputMaybe<Scalars['ID']>;
  id_gte?: InputMaybe<Scalars['ID']>;
  id_lte?: InputMaybe<Scalars['ID']>;
  id_in?: InputMaybe<Array<Scalars['ID']>>;
  id_not_in?: InputMaybe<Array<Scalars['ID']>>;
  cursor?: InputMaybe<Scalars['Int']>;
  cursor_not?: InputMaybe<Scalars['Int']>;
  cursor_gt?: InputMaybe<Scalars['Int']>;
  cursor_lt?: InputMaybe<Scalars['Int']>;
  cursor_gte?: InputMaybe<Scalars['Int']>;
  cursor_lte?: InputMaybe<Scalars['Int']>;
  cursor_in?: InputMaybe<Array<Scalars['Int']>>;
  cursor_not_in?: InputMaybe<Array<Scalars['Int']>>;
  renterAddress?: InputMaybe<Scalars['Bytes']>;
  renterAddress_not?: InputMaybe<Scalars['Bytes']>;
  renterAddress_in?: InputMaybe<Array<Scalars['Bytes']>>;
  renterAddress_not_in?: InputMaybe<Array<Scalars['Bytes']>>;
  renterAddress_contains?: InputMaybe<Scalars['Bytes']>;
  renterAddress_not_contains?: InputMaybe<Scalars['Bytes']>;
  rentDuration?: InputMaybe<Scalars['BigInt']>;
  rentDuration_not?: InputMaybe<Scalars['BigInt']>;
  rentDuration_gt?: InputMaybe<Scalars['BigInt']>;
  rentDuration_lt?: InputMaybe<Scalars['BigInt']>;
  rentDuration_gte?: InputMaybe<Scalars['BigInt']>;
  rentDuration_lte?: InputMaybe<Scalars['BigInt']>;
  rentDuration_in?: InputMaybe<Array<Scalars['BigInt']>>;
  rentDuration_not_in?: InputMaybe<Array<Scalars['BigInt']>>;
  rentedAt?: InputMaybe<Scalars['BigInt']>;
  rentedAt_not?: InputMaybe<Scalars['BigInt']>;
  rentedAt_gt?: InputMaybe<Scalars['BigInt']>;
  rentedAt_lt?: InputMaybe<Scalars['BigInt']>;
  rentedAt_gte?: InputMaybe<Scalars['BigInt']>;
  rentedAt_lte?: InputMaybe<Scalars['BigInt']>;
  rentedAt_in?: InputMaybe<Array<Scalars['BigInt']>>;
  rentedAt_not_in?: InputMaybe<Array<Scalars['BigInt']>>;
  lending?: InputMaybe<Scalars['String']>;
  lending_not?: InputMaybe<Scalars['String']>;
  lending_gt?: InputMaybe<Scalars['String']>;
  lending_lt?: InputMaybe<Scalars['String']>;
  lending_gte?: InputMaybe<Scalars['String']>;
  lending_lte?: InputMaybe<Scalars['String']>;
  lending_in?: InputMaybe<Array<Scalars['String']>>;
  lending_not_in?: InputMaybe<Array<Scalars['String']>>;
  lending_contains?: InputMaybe<Scalars['String']>;
  lending_contains_nocase?: InputMaybe<Scalars['String']>;
  lending_not_contains?: InputMaybe<Scalars['String']>;
  lending_not_contains_nocase?: InputMaybe<Scalars['String']>;
  lending_starts_with?: InputMaybe<Scalars['String']>;
  lending_starts_with_nocase?: InputMaybe<Scalars['String']>;
  lending_not_starts_with?: InputMaybe<Scalars['String']>;
  lending_not_starts_with_nocase?: InputMaybe<Scalars['String']>;
  lending_ends_with?: InputMaybe<Scalars['String']>;
  lending_ends_with_nocase?: InputMaybe<Scalars['String']>;
  lending_not_ends_with?: InputMaybe<Scalars['String']>;
  lending_not_ends_with_nocase?: InputMaybe<Scalars['String']>;
  lending_?: InputMaybe<Lending_filter>;
  nft?: InputMaybe<Scalars['String']>;
  nft_not?: InputMaybe<Scalars['String']>;
  nft_gt?: InputMaybe<Scalars['String']>;
  nft_lt?: InputMaybe<Scalars['String']>;
  nft_gte?: InputMaybe<Scalars['String']>;
  nft_lte?: InputMaybe<Scalars['String']>;
  nft_in?: InputMaybe<Array<Scalars['String']>>;
  nft_not_in?: InputMaybe<Array<Scalars['String']>>;
  nft_contains?: InputMaybe<Scalars['String']>;
  nft_contains_nocase?: InputMaybe<Scalars['String']>;
  nft_not_contains?: InputMaybe<Scalars['String']>;
  nft_not_contains_nocase?: InputMaybe<Scalars['String']>;
  nft_starts_with?: InputMaybe<Scalars['String']>;
  nft_starts_with_nocase?: InputMaybe<Scalars['String']>;
  nft_not_starts_with?: InputMaybe<Scalars['String']>;
  nft_not_starts_with_nocase?: InputMaybe<Scalars['String']>;
  nft_ends_with?: InputMaybe<Scalars['String']>;
  nft_ends_with_nocase?: InputMaybe<Scalars['String']>;
  nft_not_ends_with?: InputMaybe<Scalars['String']>;
  nft_not_ends_with_nocase?: InputMaybe<Scalars['String']>;
  nft_?: InputMaybe<Nft_filter>;
  renterUser?: InputMaybe<Scalars['String']>;
  renterUser_not?: InputMaybe<Scalars['String']>;
  renterUser_gt?: InputMaybe<Scalars['String']>;
  renterUser_lt?: InputMaybe<Scalars['String']>;
  renterUser_gte?: InputMaybe<Scalars['String']>;
  renterUser_lte?: InputMaybe<Scalars['String']>;
  renterUser_in?: InputMaybe<Array<Scalars['String']>>;
  renterUser_not_in?: InputMaybe<Array<Scalars['String']>>;
  renterUser_contains?: InputMaybe<Scalars['String']>;
  renterUser_contains_nocase?: InputMaybe<Scalars['String']>;
  renterUser_not_contains?: InputMaybe<Scalars['String']>;
  renterUser_not_contains_nocase?: InputMaybe<Scalars['String']>;
  renterUser_starts_with?: InputMaybe<Scalars['String']>;
  renterUser_starts_with_nocase?: InputMaybe<Scalars['String']>;
  renterUser_not_starts_with?: InputMaybe<Scalars['String']>;
  renterUser_not_starts_with_nocase?: InputMaybe<Scalars['String']>;
  renterUser_ends_with?: InputMaybe<Scalars['String']>;
  renterUser_ends_with_nocase?: InputMaybe<Scalars['String']>;
  renterUser_not_ends_with?: InputMaybe<Scalars['String']>;
  renterUser_not_ends_with_nocase?: InputMaybe<Scalars['String']>;
  renterUser_?: InputMaybe<User_filter>;
  /** Filter for the block changed event. */
  _change_block?: InputMaybe<BlockChangedFilter>;
};

export type Renting_orderBy =
  | 'id'
  | 'cursor'
  | 'renterAddress'
  | 'rentDuration'
  | 'rentedAt'
  | 'lending'
  | 'nft'
  | 'renterUser';

export type Subscription = {
  lending?: Maybe<Lending>;
  lendings: Array<Lending>;
  renting?: Maybe<Renting>;
  rentings: Array<Renting>;
  nft?: Maybe<Nft>;
  nfts: Array<Nft>;
  user?: Maybe<User>;
  users: Array<User>;
  lendingRentingCount?: Maybe<LendingRentingCount>;
  lendingRentingCounts: Array<LendingRentingCount>;
  counter?: Maybe<Counter>;
  counters: Array<Counter>;
  /** Access to subgraph metadata */
  _meta?: Maybe<_Meta_>;
};


export type SubscriptionlendingArgs = {
  id: Scalars['ID'];
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};


export type SubscriptionlendingsArgs = {
  skip?: InputMaybe<Scalars['Int']>;
  first?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Lending_orderBy>;
  orderDirection?: InputMaybe<OrderDirection>;
  where?: InputMaybe<Lending_filter>;
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};


export type SubscriptionrentingArgs = {
  id: Scalars['ID'];
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};


export type SubscriptionrentingsArgs = {
  skip?: InputMaybe<Scalars['Int']>;
  first?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Renting_orderBy>;
  orderDirection?: InputMaybe<OrderDirection>;
  where?: InputMaybe<Renting_filter>;
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};


export type SubscriptionnftArgs = {
  id: Scalars['ID'];
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};


export type SubscriptionnftsArgs = {
  skip?: InputMaybe<Scalars['Int']>;
  first?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Nft_orderBy>;
  orderDirection?: InputMaybe<OrderDirection>;
  where?: InputMaybe<Nft_filter>;
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};


export type SubscriptionuserArgs = {
  id: Scalars['ID'];
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};


export type SubscriptionusersArgs = {
  skip?: InputMaybe<Scalars['Int']>;
  first?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<User_orderBy>;
  orderDirection?: InputMaybe<OrderDirection>;
  where?: InputMaybe<User_filter>;
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};


export type SubscriptionlendingRentingCountArgs = {
  id: Scalars['ID'];
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};


export type SubscriptionlendingRentingCountsArgs = {
  skip?: InputMaybe<Scalars['Int']>;
  first?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<LendingRentingCount_orderBy>;
  orderDirection?: InputMaybe<OrderDirection>;
  where?: InputMaybe<LendingRentingCount_filter>;
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};


export type SubscriptioncounterArgs = {
  id: Scalars['ID'];
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};


export type SubscriptioncountersArgs = {
  skip?: InputMaybe<Scalars['Int']>;
  first?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Counter_orderBy>;
  orderDirection?: InputMaybe<OrderDirection>;
  where?: InputMaybe<Counter_filter>;
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};


export type Subscription_metaArgs = {
  block?: InputMaybe<Block_height>;
};

export type User = {
  /** id here is user's Ethereum address */
  id: Scalars['ID'];
  cursor: Scalars['Int'];
  /** each Lending and Renting in the arrays here will have DIFFERENT nftAddress and tokenId */
  lending?: Maybe<Array<Lending>>;
  renting?: Maybe<Array<Renting>>;
};


export type UserlendingArgs = {
  skip?: InputMaybe<Scalars['Int']>;
  first?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Lending_orderBy>;
  orderDirection?: InputMaybe<OrderDirection>;
  where?: InputMaybe<Lending_filter>;
};


export type UserrentingArgs = {
  skip?: InputMaybe<Scalars['Int']>;
  first?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Renting_orderBy>;
  orderDirection?: InputMaybe<OrderDirection>;
  where?: InputMaybe<Renting_filter>;
};

export type User_filter = {
  id?: InputMaybe<Scalars['ID']>;
  id_not?: InputMaybe<Scalars['ID']>;
  id_gt?: InputMaybe<Scalars['ID']>;
  id_lt?: InputMaybe<Scalars['ID']>;
  id_gte?: InputMaybe<Scalars['ID']>;
  id_lte?: InputMaybe<Scalars['ID']>;
  id_in?: InputMaybe<Array<Scalars['ID']>>;
  id_not_in?: InputMaybe<Array<Scalars['ID']>>;
  cursor?: InputMaybe<Scalars['Int']>;
  cursor_not?: InputMaybe<Scalars['Int']>;
  cursor_gt?: InputMaybe<Scalars['Int']>;
  cursor_lt?: InputMaybe<Scalars['Int']>;
  cursor_gte?: InputMaybe<Scalars['Int']>;
  cursor_lte?: InputMaybe<Scalars['Int']>;
  cursor_in?: InputMaybe<Array<Scalars['Int']>>;
  cursor_not_in?: InputMaybe<Array<Scalars['Int']>>;
  lending_?: InputMaybe<Lending_filter>;
  renting_?: InputMaybe<Renting_filter>;
  /** Filter for the block changed event. */
  _change_block?: InputMaybe<BlockChangedFilter>;
};

export type User_orderBy =
  | 'id'
  | 'cursor'
  | 'lending'
  | 'renting';

export type _Block_ = {
  /** The hash of the block */
  hash?: Maybe<Scalars['Bytes']>;
  /** The block number */
  number: Scalars['Int'];
  /** Integer representation of the timestamp stored in blocks for the chain */
  timestamp?: Maybe<Scalars['Int']>;
};

/** The type for the top-level _meta field */
export type _Meta_ = {
  /**
   * Information about a specific subgraph block. The hash of the block
   * will be null if the _meta field has a block constraint that asks for
   * a block number. It will be filled if the _meta field has no block constraint
   * and therefore asks for the latest  block
   *
   */
  block: _Block_;
  /** The deployment ID */
  deployment: Scalars['String'];
  /** If `true`, the subgraph encountered indexing errors at some past block */
  hasIndexingErrors: Scalars['Boolean'];
};

export type _SubgraphErrorPolicy_ =
  /** Data will be returned even if the subgraph has indexing errors */
  | 'allow'
  /** If the subgraph has indexing errors, data will be omitted. The default. */
  | 'deny';

  export type QuerySdk = {
      /** null **/
  lending: InContextSdkMethod<Query['lending'], QuerylendingArgs, MeshContext>,
  /** null **/
  lendings: InContextSdkMethod<Query['lendings'], QuerylendingsArgs, MeshContext>,
  /** null **/
  renting: InContextSdkMethod<Query['renting'], QueryrentingArgs, MeshContext>,
  /** null **/
  rentings: InContextSdkMethod<Query['rentings'], QueryrentingsArgs, MeshContext>,
  /** null **/
  nft: InContextSdkMethod<Query['nft'], QuerynftArgs, MeshContext>,
  /** null **/
  nfts: InContextSdkMethod<Query['nfts'], QuerynftsArgs, MeshContext>,
  /** null **/
  user: InContextSdkMethod<Query['user'], QueryuserArgs, MeshContext>,
  /** null **/
  users: InContextSdkMethod<Query['users'], QueryusersArgs, MeshContext>,
  /** null **/
  lendingRentingCount: InContextSdkMethod<Query['lendingRentingCount'], QuerylendingRentingCountArgs, MeshContext>,
  /** null **/
  lendingRentingCounts: InContextSdkMethod<Query['lendingRentingCounts'], QuerylendingRentingCountsArgs, MeshContext>,
  /** null **/
  counter: InContextSdkMethod<Query['counter'], QuerycounterArgs, MeshContext>,
  /** null **/
  counters: InContextSdkMethod<Query['counters'], QuerycountersArgs, MeshContext>,
  /** Access to subgraph metadata **/
  _meta: InContextSdkMethod<Query['_meta'], Query_metaArgs, MeshContext>
  };

  export type MutationSdk = {
    
  };

  export type SubscriptionSdk = {
      /** null **/
  lending: InContextSdkMethod<Subscription['lending'], SubscriptionlendingArgs, MeshContext>,
  /** null **/
  lendings: InContextSdkMethod<Subscription['lendings'], SubscriptionlendingsArgs, MeshContext>,
  /** null **/
  renting: InContextSdkMethod<Subscription['renting'], SubscriptionrentingArgs, MeshContext>,
  /** null **/
  rentings: InContextSdkMethod<Subscription['rentings'], SubscriptionrentingsArgs, MeshContext>,
  /** null **/
  nft: InContextSdkMethod<Subscription['nft'], SubscriptionnftArgs, MeshContext>,
  /** null **/
  nfts: InContextSdkMethod<Subscription['nfts'], SubscriptionnftsArgs, MeshContext>,
  /** null **/
  user: InContextSdkMethod<Subscription['user'], SubscriptionuserArgs, MeshContext>,
  /** null **/
  users: InContextSdkMethod<Subscription['users'], SubscriptionusersArgs, MeshContext>,
  /** null **/
  lendingRentingCount: InContextSdkMethod<Subscription['lendingRentingCount'], SubscriptionlendingRentingCountArgs, MeshContext>,
  /** null **/
  lendingRentingCounts: InContextSdkMethod<Subscription['lendingRentingCounts'], SubscriptionlendingRentingCountsArgs, MeshContext>,
  /** null **/
  counter: InContextSdkMethod<Subscription['counter'], SubscriptioncounterArgs, MeshContext>,
  /** null **/
  counters: InContextSdkMethod<Subscription['counters'], SubscriptioncountersArgs, MeshContext>,
  /** Access to subgraph metadata **/
  _meta: InContextSdkMethod<Subscription['_meta'], Subscription_metaArgs, MeshContext>
  };

  export type Context = {
      ["renft"]: { Query: QuerySdk, Mutation: MutationSdk, Subscription: SubscriptionSdk },
      
    };
}
