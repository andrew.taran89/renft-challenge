schema {
  query: Query
  subscription: Subscription
}

"Marks the GraphQL type as indexable entity.  Each type that should be an entity is required to be annotated with this directive."
directive @entity on OBJECT

"Defined a Subgraph ID for an object type"
directive @subgraphId(id: String!) on OBJECT

"creates a virtual field on the entity that may be queried but cannot be set manually through the mappings API."
directive @derivedFrom(field: String!) on FIELD_DEFINITION

scalar BigDecimal

scalar BigInt

input BlockChangedFilter {
  number_gte: Int!
}

input Block_height {
  hash: Bytes
  number: Int
  number_gte: Int
}

scalar Bytes

type Counter {
  id: ID!
  lending: Int!
  renting: Int!
  user: Int!
}

input Counter_filter {
  id: ID
  id_not: ID
  id_gt: ID
  id_lt: ID
  id_gte: ID
  id_lte: ID
  id_in: [ID!]
  id_not_in: [ID!]
  lending: Int
  lending_not: Int
  lending_gt: Int
  lending_lt: Int
  lending_gte: Int
  lending_lte: Int
  lending_in: [Int!]
  lending_not_in: [Int!]
  renting: Int
  renting_not: Int
  renting_gt: Int
  renting_lt: Int
  renting_gte: Int
  renting_lte: Int
  renting_in: [Int!]
  renting_not_in: [Int!]
  user: Int
  user_not: Int
  user_gt: Int
  user_lt: Int
  user_gte: Int
  user_lte: Int
  user_in: [Int!]
  user_not_in: [Int!]
  """Filter for the block changed event."""
  _change_block: BlockChangedFilter
}

enum Counter_orderBy {
  id
  lending
  renting
  user
}

type Lending {
  id: ID!
  cursor: Int!
  nftAddress: Bytes!
  tokenId: BigInt!
  lenderAddress: Bytes!
  maxRentDuration: BigInt!
  dailyRentPrice: Bytes!
  nftPrice: Bytes!
  paymentToken: BigInt!
  lentAmount: BigInt!
  isERC721: Boolean!
  lentAt: BigInt!
  renting: Renting
  collateralClaimed: Boolean!
  """id := nftAddresss::tokenId::lentAmount"""
  nft: Nft!
  lenderUser: User!
}

type LendingRentingCount {
  id: ID!
  lending: BigInt!
  """less or equal lending"""
  renting: BigInt!
}

input LendingRentingCount_filter {
  id: ID
  id_not: ID
  id_gt: ID
  id_lt: ID
  id_gte: ID
  id_lte: ID
  id_in: [ID!]
  id_not_in: [ID!]
  lending: BigInt
  lending_not: BigInt
  lending_gt: BigInt
  lending_lt: BigInt
  lending_gte: BigInt
  lending_lte: BigInt
  lending_in: [BigInt!]
  lending_not_in: [BigInt!]
  renting: BigInt
  renting_not: BigInt
  renting_gt: BigInt
  renting_lt: BigInt
  renting_gte: BigInt
  renting_lte: BigInt
  renting_in: [BigInt!]
  renting_not_in: [BigInt!]
  """Filter for the block changed event."""
  _change_block: BlockChangedFilter
}

enum LendingRentingCount_orderBy {
  id
  lending
  renting
}

input Lending_filter {
  id: ID
  id_not: ID
  id_gt: ID
  id_lt: ID
  id_gte: ID
  id_lte: ID
  id_in: [ID!]
  id_not_in: [ID!]
  cursor: Int
  cursor_not: Int
  cursor_gt: Int
  cursor_lt: Int
  cursor_gte: Int
  cursor_lte: Int
  cursor_in: [Int!]
  cursor_not_in: [Int!]
  nftAddress: Bytes
  nftAddress_not: Bytes
  nftAddress_in: [Bytes!]
  nftAddress_not_in: [Bytes!]
  nftAddress_contains: Bytes
  nftAddress_not_contains: Bytes
  tokenId: BigInt
  tokenId_not: BigInt
  tokenId_gt: BigInt
  tokenId_lt: BigInt
  tokenId_gte: BigInt
  tokenId_lte: BigInt
  tokenId_in: [BigInt!]
  tokenId_not_in: [BigInt!]
  lenderAddress: Bytes
  lenderAddress_not: Bytes
  lenderAddress_in: [Bytes!]
  lenderAddress_not_in: [Bytes!]
  lenderAddress_contains: Bytes
  lenderAddress_not_contains: Bytes
  maxRentDuration: BigInt
  maxRentDuration_not: BigInt
  maxRentDuration_gt: BigInt
  maxRentDuration_lt: BigInt
  maxRentDuration_gte: BigInt
  maxRentDuration_lte: BigInt
  maxRentDuration_in: [BigInt!]
  maxRentDuration_not_in: [BigInt!]
  dailyRentPrice: Bytes
  dailyRentPrice_not: Bytes
  dailyRentPrice_in: [Bytes!]
  dailyRentPrice_not_in: [Bytes!]
  dailyRentPrice_contains: Bytes
  dailyRentPrice_not_contains: Bytes
  nftPrice: Bytes
  nftPrice_not: Bytes
  nftPrice_in: [Bytes!]
  nftPrice_not_in: [Bytes!]
  nftPrice_contains: Bytes
  nftPrice_not_contains: Bytes
  paymentToken: BigInt
  paymentToken_not: BigInt
  paymentToken_gt: BigInt
  paymentToken_lt: BigInt
  paymentToken_gte: BigInt
  paymentToken_lte: BigInt
  paymentToken_in: [BigInt!]
  paymentToken_not_in: [BigInt!]
  lentAmount: BigInt
  lentAmount_not: BigInt
  lentAmount_gt: BigInt
  lentAmount_lt: BigInt
  lentAmount_gte: BigInt
  lentAmount_lte: BigInt
  lentAmount_in: [BigInt!]
  lentAmount_not_in: [BigInt!]
  isERC721: Boolean
  isERC721_not: Boolean
  isERC721_in: [Boolean!]
  isERC721_not_in: [Boolean!]
  lentAt: BigInt
  lentAt_not: BigInt
  lentAt_gt: BigInt
  lentAt_lt: BigInt
  lentAt_gte: BigInt
  lentAt_lte: BigInt
  lentAt_in: [BigInt!]
  lentAt_not_in: [BigInt!]
  renting: String
  renting_not: String
  renting_gt: String
  renting_lt: String
  renting_gte: String
  renting_lte: String
  renting_in: [String!]
  renting_not_in: [String!]
  renting_contains: String
  renting_contains_nocase: String
  renting_not_contains: String
  renting_not_contains_nocase: String
  renting_starts_with: String
  renting_starts_with_nocase: String
  renting_not_starts_with: String
  renting_not_starts_with_nocase: String
  renting_ends_with: String
  renting_ends_with_nocase: String
  renting_not_ends_with: String
  renting_not_ends_with_nocase: String
  renting_: Renting_filter
  collateralClaimed: Boolean
  collateralClaimed_not: Boolean
  collateralClaimed_in: [Boolean!]
  collateralClaimed_not_in: [Boolean!]
  nft: String
  nft_not: String
  nft_gt: String
  nft_lt: String
  nft_gte: String
  nft_lte: String
  nft_in: [String!]
  nft_not_in: [String!]
  nft_contains: String
  nft_contains_nocase: String
  nft_not_contains: String
  nft_not_contains_nocase: String
  nft_starts_with: String
  nft_starts_with_nocase: String
  nft_not_starts_with: String
  nft_not_starts_with_nocase: String
  nft_ends_with: String
  nft_ends_with_nocase: String
  nft_not_ends_with: String
  nft_not_ends_with_nocase: String
  nft_: Nft_filter
  lenderUser: String
  lenderUser_not: String
  lenderUser_gt: String
  lenderUser_lt: String
  lenderUser_gte: String
  lenderUser_lte: String
  lenderUser_in: [String!]
  lenderUser_not_in: [String!]
  lenderUser_contains: String
  lenderUser_contains_nocase: String
  lenderUser_not_contains: String
  lenderUser_not_contains_nocase: String
  lenderUser_starts_with: String
  lenderUser_starts_with_nocase: String
  lenderUser_not_starts_with: String
  lenderUser_not_starts_with_nocase: String
  lenderUser_ends_with: String
  lenderUser_ends_with_nocase: String
  lenderUser_not_ends_with: String
  lenderUser_not_ends_with_nocase: String
  lenderUser_: User_filter
  """Filter for the block changed event."""
  _change_block: BlockChangedFilter
}

enum Lending_orderBy {
  id
  cursor
  nftAddress
  tokenId
  lenderAddress
  maxRentDuration
  dailyRentPrice
  nftPrice
  paymentToken
  lentAmount
  isERC721
  lentAt
  renting
  collateralClaimed
  nft
  lenderUser
}

type Nft {
  """id is nftAddress::tokenId::lentAmount"""
  id: ID!
  """
  each Lending and Renting in the arrays here will have the SAME nftAddress and tokenId!!!!!! As per the id of this entity
  """
  lending(skip: Int = 0, first: Int = 100, orderBy: Lending_orderBy, orderDirection: OrderDirection, where: Lending_filter): [Lending!]!
  renting(skip: Int = 0, first: Int = 100, orderBy: Renting_orderBy, orderDirection: OrderDirection, where: Renting_filter): [Renting!]
}

input Nft_filter {
  id: ID
  id_not: ID
  id_gt: ID
  id_lt: ID
  id_gte: ID
  id_lte: ID
  id_in: [ID!]
  id_not_in: [ID!]
  lending_: Lending_filter
  renting_: Renting_filter
  """Filter for the block changed event."""
  _change_block: BlockChangedFilter
}

enum Nft_orderBy {
  id
  lending
  renting
}

"""Defines the order direction, either ascending or descending"""
enum OrderDirection {
  asc
  desc
}

type Query {
  lending(
    id: ID!
    """
    The block at which the query should be executed. Can either be a `{ hash: Bytes }` value containing a block hash, a `{ number: Int }` containing the block number, or a `{ number_gte: Int }` containing the minimum block number. In the case of `number_gte`, the query will be executed on the latest block only if the subgraph has progressed to or past the minimum block number. Defaults to the latest block when omitted.
    """
    block: Block_height
    """
    Set to `allow` to receive data even if the subgraph has skipped over errors while syncing.
    """
    subgraphError: _SubgraphErrorPolicy_! = deny
  ): Lending
  lendings(
    skip: Int = 0
    first: Int = 100
    orderBy: Lending_orderBy
    orderDirection: OrderDirection
    where: Lending_filter
    """
    The block at which the query should be executed. Can either be a `{ hash: Bytes }` value containing a block hash, a `{ number: Int }` containing the block number, or a `{ number_gte: Int }` containing the minimum block number. In the case of `number_gte`, the query will be executed on the latest block only if the subgraph has progressed to or past the minimum block number. Defaults to the latest block when omitted.
    """
    block: Block_height
    """
    Set to `allow` to receive data even if the subgraph has skipped over errors while syncing.
    """
    subgraphError: _SubgraphErrorPolicy_! = deny
  ): [Lending!]!
  renting(
    id: ID!
    """
    The block at which the query should be executed. Can either be a `{ hash: Bytes }` value containing a block hash, a `{ number: Int }` containing the block number, or a `{ number_gte: Int }` containing the minimum block number. In the case of `number_gte`, the query will be executed on the latest block only if the subgraph has progressed to or past the minimum block number. Defaults to the latest block when omitted.
    """
    block: Block_height
    """
    Set to `allow` to receive data even if the subgraph has skipped over errors while syncing.
    """
    subgraphError: _SubgraphErrorPolicy_! = deny
  ): Renting
  rentings(
    skip: Int = 0
    first: Int = 100
    orderBy: Renting_orderBy
    orderDirection: OrderDirection
    where: Renting_filter
    """
    The block at which the query should be executed. Can either be a `{ hash: Bytes }` value containing a block hash, a `{ number: Int }` containing the block number, or a `{ number_gte: Int }` containing the minimum block number. In the case of `number_gte`, the query will be executed on the latest block only if the subgraph has progressed to or past the minimum block number. Defaults to the latest block when omitted.
    """
    block: Block_height
    """
    Set to `allow` to receive data even if the subgraph has skipped over errors while syncing.
    """
    subgraphError: _SubgraphErrorPolicy_! = deny
  ): [Renting!]!
  nft(
    id: ID!
    """
    The block at which the query should be executed. Can either be a `{ hash: Bytes }` value containing a block hash, a `{ number: Int }` containing the block number, or a `{ number_gte: Int }` containing the minimum block number. In the case of `number_gte`, the query will be executed on the latest block only if the subgraph has progressed to or past the minimum block number. Defaults to the latest block when omitted.
    """
    block: Block_height
    """
    Set to `allow` to receive data even if the subgraph has skipped over errors while syncing.
    """
    subgraphError: _SubgraphErrorPolicy_! = deny
  ): Nft
  nfts(
    skip: Int = 0
    first: Int = 100
    orderBy: Nft_orderBy
    orderDirection: OrderDirection
    where: Nft_filter
    """
    The block at which the query should be executed. Can either be a `{ hash: Bytes }` value containing a block hash, a `{ number: Int }` containing the block number, or a `{ number_gte: Int }` containing the minimum block number. In the case of `number_gte`, the query will be executed on the latest block only if the subgraph has progressed to or past the minimum block number. Defaults to the latest block when omitted.
    """
    block: Block_height
    """
    Set to `allow` to receive data even if the subgraph has skipped over errors while syncing.
    """
    subgraphError: _SubgraphErrorPolicy_! = deny
  ): [Nft!]!
  user(
    id: ID!
    """
    The block at which the query should be executed. Can either be a `{ hash: Bytes }` value containing a block hash, a `{ number: Int }` containing the block number, or a `{ number_gte: Int }` containing the minimum block number. In the case of `number_gte`, the query will be executed on the latest block only if the subgraph has progressed to or past the minimum block number. Defaults to the latest block when omitted.
    """
    block: Block_height
    """
    Set to `allow` to receive data even if the subgraph has skipped over errors while syncing.
    """
    subgraphError: _SubgraphErrorPolicy_! = deny
  ): User
  users(
    skip: Int = 0
    first: Int = 100
    orderBy: User_orderBy
    orderDirection: OrderDirection
    where: User_filter
    """
    The block at which the query should be executed. Can either be a `{ hash: Bytes }` value containing a block hash, a `{ number: Int }` containing the block number, or a `{ number_gte: Int }` containing the minimum block number. In the case of `number_gte`, the query will be executed on the latest block only if the subgraph has progressed to or past the minimum block number. Defaults to the latest block when omitted.
    """
    block: Block_height
    """
    Set to `allow` to receive data even if the subgraph has skipped over errors while syncing.
    """
    subgraphError: _SubgraphErrorPolicy_! = deny
  ): [User!]!
  lendingRentingCount(
    id: ID!
    """
    The block at which the query should be executed. Can either be a `{ hash: Bytes }` value containing a block hash, a `{ number: Int }` containing the block number, or a `{ number_gte: Int }` containing the minimum block number. In the case of `number_gte`, the query will be executed on the latest block only if the subgraph has progressed to or past the minimum block number. Defaults to the latest block when omitted.
    """
    block: Block_height
    """
    Set to `allow` to receive data even if the subgraph has skipped over errors while syncing.
    """
    subgraphError: _SubgraphErrorPolicy_! = deny
  ): LendingRentingCount
  lendingRentingCounts(
    skip: Int = 0
    first: Int = 100
    orderBy: LendingRentingCount_orderBy
    orderDirection: OrderDirection
    where: LendingRentingCount_filter
    """
    The block at which the query should be executed. Can either be a `{ hash: Bytes }` value containing a block hash, a `{ number: Int }` containing the block number, or a `{ number_gte: Int }` containing the minimum block number. In the case of `number_gte`, the query will be executed on the latest block only if the subgraph has progressed to or past the minimum block number. Defaults to the latest block when omitted.
    """
    block: Block_height
    """
    Set to `allow` to receive data even if the subgraph has skipped over errors while syncing.
    """
    subgraphError: _SubgraphErrorPolicy_! = deny
  ): [LendingRentingCount!]!
  counter(
    id: ID!
    """
    The block at which the query should be executed. Can either be a `{ hash: Bytes }` value containing a block hash, a `{ number: Int }` containing the block number, or a `{ number_gte: Int }` containing the minimum block number. In the case of `number_gte`, the query will be executed on the latest block only if the subgraph has progressed to or past the minimum block number. Defaults to the latest block when omitted.
    """
    block: Block_height
    """
    Set to `allow` to receive data even if the subgraph has skipped over errors while syncing.
    """
    subgraphError: _SubgraphErrorPolicy_! = deny
  ): Counter
  counters(
    skip: Int = 0
    first: Int = 100
    orderBy: Counter_orderBy
    orderDirection: OrderDirection
    where: Counter_filter
    """
    The block at which the query should be executed. Can either be a `{ hash: Bytes }` value containing a block hash, a `{ number: Int }` containing the block number, or a `{ number_gte: Int }` containing the minimum block number. In the case of `number_gte`, the query will be executed on the latest block only if the subgraph has progressed to or past the minimum block number. Defaults to the latest block when omitted.
    """
    block: Block_height
    """
    Set to `allow` to receive data even if the subgraph has skipped over errors while syncing.
    """
    subgraphError: _SubgraphErrorPolicy_! = deny
  ): [Counter!]!
  """Access to subgraph metadata"""
  _meta(block: Block_height): _Meta_
}

type Renting {
  id: ID!
  cursor: Int!
  renterAddress: Bytes!
  rentDuration: BigInt!
  rentedAt: BigInt!
  lending: Lending!
  """id := nftAddress::tokenId::lentAmount"""
  nft: Nft!
  renterUser: User!
}

input Renting_filter {
  id: ID
  id_not: ID
  id_gt: ID
  id_lt: ID
  id_gte: ID
  id_lte: ID
  id_in: [ID!]
  id_not_in: [ID!]
  cursor: Int
  cursor_not: Int
  cursor_gt: Int
  cursor_lt: Int
  cursor_gte: Int
  cursor_lte: Int
  cursor_in: [Int!]
  cursor_not_in: [Int!]
  renterAddress: Bytes
  renterAddress_not: Bytes
  renterAddress_in: [Bytes!]
  renterAddress_not_in: [Bytes!]
  renterAddress_contains: Bytes
  renterAddress_not_contains: Bytes
  rentDuration: BigInt
  rentDuration_not: BigInt
  rentDuration_gt: BigInt
  rentDuration_lt: BigInt
  rentDuration_gte: BigInt
  rentDuration_lte: BigInt
  rentDuration_in: [BigInt!]
  rentDuration_not_in: [BigInt!]
  rentedAt: BigInt
  rentedAt_not: BigInt
  rentedAt_gt: BigInt
  rentedAt_lt: BigInt
  rentedAt_gte: BigInt
  rentedAt_lte: BigInt
  rentedAt_in: [BigInt!]
  rentedAt_not_in: [BigInt!]
  lending: String
  lending_not: String
  lending_gt: String
  lending_lt: String
  lending_gte: String
  lending_lte: String
  lending_in: [String!]
  lending_not_in: [String!]
  lending_contains: String
  lending_contains_nocase: String
  lending_not_contains: String
  lending_not_contains_nocase: String
  lending_starts_with: String
  lending_starts_with_nocase: String
  lending_not_starts_with: String
  lending_not_starts_with_nocase: String
  lending_ends_with: String
  lending_ends_with_nocase: String
  lending_not_ends_with: String
  lending_not_ends_with_nocase: String
  lending_: Lending_filter
  nft: String
  nft_not: String
  nft_gt: String
  nft_lt: String
  nft_gte: String
  nft_lte: String
  nft_in: [String!]
  nft_not_in: [String!]
  nft_contains: String
  nft_contains_nocase: String
  nft_not_contains: String
  nft_not_contains_nocase: String
  nft_starts_with: String
  nft_starts_with_nocase: String
  nft_not_starts_with: String
  nft_not_starts_with_nocase: String
  nft_ends_with: String
  nft_ends_with_nocase: String
  nft_not_ends_with: String
  nft_not_ends_with_nocase: String
  nft_: Nft_filter
  renterUser: String
  renterUser_not: String
  renterUser_gt: String
  renterUser_lt: String
  renterUser_gte: String
  renterUser_lte: String
  renterUser_in: [String!]
  renterUser_not_in: [String!]
  renterUser_contains: String
  renterUser_contains_nocase: String
  renterUser_not_contains: String
  renterUser_not_contains_nocase: String
  renterUser_starts_with: String
  renterUser_starts_with_nocase: String
  renterUser_not_starts_with: String
  renterUser_not_starts_with_nocase: String
  renterUser_ends_with: String
  renterUser_ends_with_nocase: String
  renterUser_not_ends_with: String
  renterUser_not_ends_with_nocase: String
  renterUser_: User_filter
  """Filter for the block changed event."""
  _change_block: BlockChangedFilter
}

enum Renting_orderBy {
  id
  cursor
  renterAddress
  rentDuration
  rentedAt
  lending
  nft
  renterUser
}

type Subscription {
  lending(
    id: ID!
    """
    The block at which the query should be executed. Can either be a `{ hash: Bytes }` value containing a block hash, a `{ number: Int }` containing the block number, or a `{ number_gte: Int }` containing the minimum block number. In the case of `number_gte`, the query will be executed on the latest block only if the subgraph has progressed to or past the minimum block number. Defaults to the latest block when omitted.
    """
    block: Block_height
    """
    Set to `allow` to receive data even if the subgraph has skipped over errors while syncing.
    """
    subgraphError: _SubgraphErrorPolicy_! = deny
  ): Lending
  lendings(
    skip: Int = 0
    first: Int = 100
    orderBy: Lending_orderBy
    orderDirection: OrderDirection
    where: Lending_filter
    """
    The block at which the query should be executed. Can either be a `{ hash: Bytes }` value containing a block hash, a `{ number: Int }` containing the block number, or a `{ number_gte: Int }` containing the minimum block number. In the case of `number_gte`, the query will be executed on the latest block only if the subgraph has progressed to or past the minimum block number. Defaults to the latest block when omitted.
    """
    block: Block_height
    """
    Set to `allow` to receive data even if the subgraph has skipped over errors while syncing.
    """
    subgraphError: _SubgraphErrorPolicy_! = deny
  ): [Lending!]!
  renting(
    id: ID!
    """
    The block at which the query should be executed. Can either be a `{ hash: Bytes }` value containing a block hash, a `{ number: Int }` containing the block number, or a `{ number_gte: Int }` containing the minimum block number. In the case of `number_gte`, the query will be executed on the latest block only if the subgraph has progressed to or past the minimum block number. Defaults to the latest block when omitted.
    """
    block: Block_height
    """
    Set to `allow` to receive data even if the subgraph has skipped over errors while syncing.
    """
    subgraphError: _SubgraphErrorPolicy_! = deny
  ): Renting
  rentings(
    skip: Int = 0
    first: Int = 100
    orderBy: Renting_orderBy
    orderDirection: OrderDirection
    where: Renting_filter
    """
    The block at which the query should be executed. Can either be a `{ hash: Bytes }` value containing a block hash, a `{ number: Int }` containing the block number, or a `{ number_gte: Int }` containing the minimum block number. In the case of `number_gte`, the query will be executed on the latest block only if the subgraph has progressed to or past the minimum block number. Defaults to the latest block when omitted.
    """
    block: Block_height
    """
    Set to `allow` to receive data even if the subgraph has skipped over errors while syncing.
    """
    subgraphError: _SubgraphErrorPolicy_! = deny
  ): [Renting!]!
  nft(
    id: ID!
    """
    The block at which the query should be executed. Can either be a `{ hash: Bytes }` value containing a block hash, a `{ number: Int }` containing the block number, or a `{ number_gte: Int }` containing the minimum block number. In the case of `number_gte`, the query will be executed on the latest block only if the subgraph has progressed to or past the minimum block number. Defaults to the latest block when omitted.
    """
    block: Block_height
    """
    Set to `allow` to receive data even if the subgraph has skipped over errors while syncing.
    """
    subgraphError: _SubgraphErrorPolicy_! = deny
  ): Nft
  nfts(
    skip: Int = 0
    first: Int = 100
    orderBy: Nft_orderBy
    orderDirection: OrderDirection
    where: Nft_filter
    """
    The block at which the query should be executed. Can either be a `{ hash: Bytes }` value containing a block hash, a `{ number: Int }` containing the block number, or a `{ number_gte: Int }` containing the minimum block number. In the case of `number_gte`, the query will be executed on the latest block only if the subgraph has progressed to or past the minimum block number. Defaults to the latest block when omitted.
    """
    block: Block_height
    """
    Set to `allow` to receive data even if the subgraph has skipped over errors while syncing.
    """
    subgraphError: _SubgraphErrorPolicy_! = deny
  ): [Nft!]!
  user(
    id: ID!
    """
    The block at which the query should be executed. Can either be a `{ hash: Bytes }` value containing a block hash, a `{ number: Int }` containing the block number, or a `{ number_gte: Int }` containing the minimum block number. In the case of `number_gte`, the query will be executed on the latest block only if the subgraph has progressed to or past the minimum block number. Defaults to the latest block when omitted.
    """
    block: Block_height
    """
    Set to `allow` to receive data even if the subgraph has skipped over errors while syncing.
    """
    subgraphError: _SubgraphErrorPolicy_! = deny
  ): User
  users(
    skip: Int = 0
    first: Int = 100
    orderBy: User_orderBy
    orderDirection: OrderDirection
    where: User_filter
    """
    The block at which the query should be executed. Can either be a `{ hash: Bytes }` value containing a block hash, a `{ number: Int }` containing the block number, or a `{ number_gte: Int }` containing the minimum block number. In the case of `number_gte`, the query will be executed on the latest block only if the subgraph has progressed to or past the minimum block number. Defaults to the latest block when omitted.
    """
    block: Block_height
    """
    Set to `allow` to receive data even if the subgraph has skipped over errors while syncing.
    """
    subgraphError: _SubgraphErrorPolicy_! = deny
  ): [User!]!
  lendingRentingCount(
    id: ID!
    """
    The block at which the query should be executed. Can either be a `{ hash: Bytes }` value containing a block hash, a `{ number: Int }` containing the block number, or a `{ number_gte: Int }` containing the minimum block number. In the case of `number_gte`, the query will be executed on the latest block only if the subgraph has progressed to or past the minimum block number. Defaults to the latest block when omitted.
    """
    block: Block_height
    """
    Set to `allow` to receive data even if the subgraph has skipped over errors while syncing.
    """
    subgraphError: _SubgraphErrorPolicy_! = deny
  ): LendingRentingCount
  lendingRentingCounts(
    skip: Int = 0
    first: Int = 100
    orderBy: LendingRentingCount_orderBy
    orderDirection: OrderDirection
    where: LendingRentingCount_filter
    """
    The block at which the query should be executed. Can either be a `{ hash: Bytes }` value containing a block hash, a `{ number: Int }` containing the block number, or a `{ number_gte: Int }` containing the minimum block number. In the case of `number_gte`, the query will be executed on the latest block only if the subgraph has progressed to or past the minimum block number. Defaults to the latest block when omitted.
    """
    block: Block_height
    """
    Set to `allow` to receive data even if the subgraph has skipped over errors while syncing.
    """
    subgraphError: _SubgraphErrorPolicy_! = deny
  ): [LendingRentingCount!]!
  counter(
    id: ID!
    """
    The block at which the query should be executed. Can either be a `{ hash: Bytes }` value containing a block hash, a `{ number: Int }` containing the block number, or a `{ number_gte: Int }` containing the minimum block number. In the case of `number_gte`, the query will be executed on the latest block only if the subgraph has progressed to or past the minimum block number. Defaults to the latest block when omitted.
    """
    block: Block_height
    """
    Set to `allow` to receive data even if the subgraph has skipped over errors while syncing.
    """
    subgraphError: _SubgraphErrorPolicy_! = deny
  ): Counter
  counters(
    skip: Int = 0
    first: Int = 100
    orderBy: Counter_orderBy
    orderDirection: OrderDirection
    where: Counter_filter
    """
    The block at which the query should be executed. Can either be a `{ hash: Bytes }` value containing a block hash, a `{ number: Int }` containing the block number, or a `{ number_gte: Int }` containing the minimum block number. In the case of `number_gte`, the query will be executed on the latest block only if the subgraph has progressed to or past the minimum block number. Defaults to the latest block when omitted.
    """
    block: Block_height
    """
    Set to `allow` to receive data even if the subgraph has skipped over errors while syncing.
    """
    subgraphError: _SubgraphErrorPolicy_! = deny
  ): [Counter!]!
  """Access to subgraph metadata"""
  _meta(block: Block_height): _Meta_
}

type User {
  """id here is user's Ethereum address"""
  id: ID!
  cursor: Int!
  """
  each Lending and Renting in the arrays here will have DIFFERENT nftAddress and tokenId
  """
  lending(skip: Int = 0, first: Int = 100, orderBy: Lending_orderBy, orderDirection: OrderDirection, where: Lending_filter): [Lending!]
  renting(skip: Int = 0, first: Int = 100, orderBy: Renting_orderBy, orderDirection: OrderDirection, where: Renting_filter): [Renting!]
}

input User_filter {
  id: ID
  id_not: ID
  id_gt: ID
  id_lt: ID
  id_gte: ID
  id_lte: ID
  id_in: [ID!]
  id_not_in: [ID!]
  cursor: Int
  cursor_not: Int
  cursor_gt: Int
  cursor_lt: Int
  cursor_gte: Int
  cursor_lte: Int
  cursor_in: [Int!]
  cursor_not_in: [Int!]
  lending_: Lending_filter
  renting_: Renting_filter
  """Filter for the block changed event."""
  _change_block: BlockChangedFilter
}

enum User_orderBy {
  id
  cursor
  lending
  renting
}

type _Block_ {
  """The hash of the block"""
  hash: Bytes
  """The block number"""
  number: Int!
  """Integer representation of the timestamp stored in blocks for the chain"""
  timestamp: Int
}

"""The type for the top-level _meta field"""
type _Meta_ {
  """
  Information about a specific subgraph block. The hash of the block
  will be null if the _meta field has a block constraint that asks for
  a block number. It will be filled if the _meta field has no block constraint
  and therefore asks for the latest  block
  
  """
  block: _Block_!
  """The deployment ID"""
  deployment: String!
  """If `true`, the subgraph encountered indexing errors at some past block"""
  hasIndexingErrors: Boolean!
}

enum _SubgraphErrorPolicy_ {
  """Data will be returned even if the subgraph has indexing errors"""
  allow
  """
  If the subgraph has indexing errors, data will be omitted. The default.
  """
  deny
}