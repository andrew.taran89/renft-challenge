// @ts-nocheck
import { GraphQLResolveInfo, SelectionSetNode, FieldNode, GraphQLScalarType, GraphQLScalarTypeConfig } from 'graphql';
import { TypedDocumentNode as DocumentNode } from '@graphql-typed-document-node/core';
import { gql } from '@graphql-mesh/utils';

import type { GetMeshOptions } from '@graphql-mesh/runtime';
import type { YamlConfig } from '@graphql-mesh/types';
import { PubSub } from '@graphql-mesh/utils';
import { DefaultLogger } from '@graphql-mesh/utils';
import MeshCache from "@graphql-mesh/cache-localforage";
import { fetch as fetchFn } from '@whatwg-node/fetch';

import { MeshResolvedSource } from '@graphql-mesh/runtime';
import { MeshTransform, MeshPlugin } from '@graphql-mesh/types';
import GraphqlHandler from "@graphql-mesh/graphql"
import BareMerger from "@graphql-mesh/merger-bare";
import { printWithCache } from '@graphql-mesh/utils';
import { createMeshHTTPHandler, MeshHTTPHandler } from '@graphql-mesh/http';
import { getMesh, ExecuteMeshFn, SubscribeMeshFn, MeshContext as BaseMeshContext, MeshInstance } from '@graphql-mesh/runtime';
import { MeshStore, FsStoreStorageAdapter } from '@graphql-mesh/store';
import { path as pathModule } from '@graphql-mesh/cross-helpers';
import { ImportFn } from '@graphql-mesh/types';
import type { RenftTypes } from './sources/renft/types';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type RequireFields<T, K extends keyof T> = Omit<T, K> & { [P in K]-?: NonNullable<T[P]> };



/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  BigDecimal: any;
  BigInt: any;
  Bytes: any;
};

export type BlockChangedFilter = {
  number_gte: Scalars['Int'];
};

export type Block_height = {
  hash?: InputMaybe<Scalars['Bytes']>;
  number?: InputMaybe<Scalars['Int']>;
  number_gte?: InputMaybe<Scalars['Int']>;
};

export type Counter = {
  id: Scalars['ID'];
  lending: Scalars['Int'];
  renting: Scalars['Int'];
  user: Scalars['Int'];
};

export type Counter_filter = {
  id?: InputMaybe<Scalars['ID']>;
  id_not?: InputMaybe<Scalars['ID']>;
  id_gt?: InputMaybe<Scalars['ID']>;
  id_lt?: InputMaybe<Scalars['ID']>;
  id_gte?: InputMaybe<Scalars['ID']>;
  id_lte?: InputMaybe<Scalars['ID']>;
  id_in?: InputMaybe<Array<Scalars['ID']>>;
  id_not_in?: InputMaybe<Array<Scalars['ID']>>;
  lending?: InputMaybe<Scalars['Int']>;
  lending_not?: InputMaybe<Scalars['Int']>;
  lending_gt?: InputMaybe<Scalars['Int']>;
  lending_lt?: InputMaybe<Scalars['Int']>;
  lending_gte?: InputMaybe<Scalars['Int']>;
  lending_lte?: InputMaybe<Scalars['Int']>;
  lending_in?: InputMaybe<Array<Scalars['Int']>>;
  lending_not_in?: InputMaybe<Array<Scalars['Int']>>;
  renting?: InputMaybe<Scalars['Int']>;
  renting_not?: InputMaybe<Scalars['Int']>;
  renting_gt?: InputMaybe<Scalars['Int']>;
  renting_lt?: InputMaybe<Scalars['Int']>;
  renting_gte?: InputMaybe<Scalars['Int']>;
  renting_lte?: InputMaybe<Scalars['Int']>;
  renting_in?: InputMaybe<Array<Scalars['Int']>>;
  renting_not_in?: InputMaybe<Array<Scalars['Int']>>;
  user?: InputMaybe<Scalars['Int']>;
  user_not?: InputMaybe<Scalars['Int']>;
  user_gt?: InputMaybe<Scalars['Int']>;
  user_lt?: InputMaybe<Scalars['Int']>;
  user_gte?: InputMaybe<Scalars['Int']>;
  user_lte?: InputMaybe<Scalars['Int']>;
  user_in?: InputMaybe<Array<Scalars['Int']>>;
  user_not_in?: InputMaybe<Array<Scalars['Int']>>;
  /** Filter for the block changed event. */
  _change_block?: InputMaybe<BlockChangedFilter>;
};

export type Counter_orderBy =
  | 'id'
  | 'lending'
  | 'renting'
  | 'user';

export type Lending = {
  id: Scalars['ID'];
  cursor: Scalars['Int'];
  nftAddress: Scalars['Bytes'];
  tokenId: Scalars['BigInt'];
  lenderAddress: Scalars['Bytes'];
  maxRentDuration: Scalars['BigInt'];
  dailyRentPrice: Scalars['Bytes'];
  nftPrice: Scalars['Bytes'];
  paymentToken: Scalars['BigInt'];
  lentAmount: Scalars['BigInt'];
  isERC721: Scalars['Boolean'];
  lentAt: Scalars['BigInt'];
  renting?: Maybe<Renting>;
  collateralClaimed: Scalars['Boolean'];
  /** id := nftAddresss::tokenId::lentAmount */
  nft: Nft;
  lenderUser: User;
};

export type LendingRentingCount = {
  id: Scalars['ID'];
  lending: Scalars['BigInt'];
  /** less or equal lending */
  renting: Scalars['BigInt'];
};

export type LendingRentingCount_filter = {
  id?: InputMaybe<Scalars['ID']>;
  id_not?: InputMaybe<Scalars['ID']>;
  id_gt?: InputMaybe<Scalars['ID']>;
  id_lt?: InputMaybe<Scalars['ID']>;
  id_gte?: InputMaybe<Scalars['ID']>;
  id_lte?: InputMaybe<Scalars['ID']>;
  id_in?: InputMaybe<Array<Scalars['ID']>>;
  id_not_in?: InputMaybe<Array<Scalars['ID']>>;
  lending?: InputMaybe<Scalars['BigInt']>;
  lending_not?: InputMaybe<Scalars['BigInt']>;
  lending_gt?: InputMaybe<Scalars['BigInt']>;
  lending_lt?: InputMaybe<Scalars['BigInt']>;
  lending_gte?: InputMaybe<Scalars['BigInt']>;
  lending_lte?: InputMaybe<Scalars['BigInt']>;
  lending_in?: InputMaybe<Array<Scalars['BigInt']>>;
  lending_not_in?: InputMaybe<Array<Scalars['BigInt']>>;
  renting?: InputMaybe<Scalars['BigInt']>;
  renting_not?: InputMaybe<Scalars['BigInt']>;
  renting_gt?: InputMaybe<Scalars['BigInt']>;
  renting_lt?: InputMaybe<Scalars['BigInt']>;
  renting_gte?: InputMaybe<Scalars['BigInt']>;
  renting_lte?: InputMaybe<Scalars['BigInt']>;
  renting_in?: InputMaybe<Array<Scalars['BigInt']>>;
  renting_not_in?: InputMaybe<Array<Scalars['BigInt']>>;
  /** Filter for the block changed event. */
  _change_block?: InputMaybe<BlockChangedFilter>;
};

export type LendingRentingCount_orderBy =
  | 'id'
  | 'lending'
  | 'renting';

export type Lending_filter = {
  id?: InputMaybe<Scalars['ID']>;
  id_not?: InputMaybe<Scalars['ID']>;
  id_gt?: InputMaybe<Scalars['ID']>;
  id_lt?: InputMaybe<Scalars['ID']>;
  id_gte?: InputMaybe<Scalars['ID']>;
  id_lte?: InputMaybe<Scalars['ID']>;
  id_in?: InputMaybe<Array<Scalars['ID']>>;
  id_not_in?: InputMaybe<Array<Scalars['ID']>>;
  cursor?: InputMaybe<Scalars['Int']>;
  cursor_not?: InputMaybe<Scalars['Int']>;
  cursor_gt?: InputMaybe<Scalars['Int']>;
  cursor_lt?: InputMaybe<Scalars['Int']>;
  cursor_gte?: InputMaybe<Scalars['Int']>;
  cursor_lte?: InputMaybe<Scalars['Int']>;
  cursor_in?: InputMaybe<Array<Scalars['Int']>>;
  cursor_not_in?: InputMaybe<Array<Scalars['Int']>>;
  nftAddress?: InputMaybe<Scalars['Bytes']>;
  nftAddress_not?: InputMaybe<Scalars['Bytes']>;
  nftAddress_in?: InputMaybe<Array<Scalars['Bytes']>>;
  nftAddress_not_in?: InputMaybe<Array<Scalars['Bytes']>>;
  nftAddress_contains?: InputMaybe<Scalars['Bytes']>;
  nftAddress_not_contains?: InputMaybe<Scalars['Bytes']>;
  tokenId?: InputMaybe<Scalars['BigInt']>;
  tokenId_not?: InputMaybe<Scalars['BigInt']>;
  tokenId_gt?: InputMaybe<Scalars['BigInt']>;
  tokenId_lt?: InputMaybe<Scalars['BigInt']>;
  tokenId_gte?: InputMaybe<Scalars['BigInt']>;
  tokenId_lte?: InputMaybe<Scalars['BigInt']>;
  tokenId_in?: InputMaybe<Array<Scalars['BigInt']>>;
  tokenId_not_in?: InputMaybe<Array<Scalars['BigInt']>>;
  lenderAddress?: InputMaybe<Scalars['Bytes']>;
  lenderAddress_not?: InputMaybe<Scalars['Bytes']>;
  lenderAddress_in?: InputMaybe<Array<Scalars['Bytes']>>;
  lenderAddress_not_in?: InputMaybe<Array<Scalars['Bytes']>>;
  lenderAddress_contains?: InputMaybe<Scalars['Bytes']>;
  lenderAddress_not_contains?: InputMaybe<Scalars['Bytes']>;
  maxRentDuration?: InputMaybe<Scalars['BigInt']>;
  maxRentDuration_not?: InputMaybe<Scalars['BigInt']>;
  maxRentDuration_gt?: InputMaybe<Scalars['BigInt']>;
  maxRentDuration_lt?: InputMaybe<Scalars['BigInt']>;
  maxRentDuration_gte?: InputMaybe<Scalars['BigInt']>;
  maxRentDuration_lte?: InputMaybe<Scalars['BigInt']>;
  maxRentDuration_in?: InputMaybe<Array<Scalars['BigInt']>>;
  maxRentDuration_not_in?: InputMaybe<Array<Scalars['BigInt']>>;
  dailyRentPrice?: InputMaybe<Scalars['Bytes']>;
  dailyRentPrice_not?: InputMaybe<Scalars['Bytes']>;
  dailyRentPrice_in?: InputMaybe<Array<Scalars['Bytes']>>;
  dailyRentPrice_not_in?: InputMaybe<Array<Scalars['Bytes']>>;
  dailyRentPrice_contains?: InputMaybe<Scalars['Bytes']>;
  dailyRentPrice_not_contains?: InputMaybe<Scalars['Bytes']>;
  nftPrice?: InputMaybe<Scalars['Bytes']>;
  nftPrice_not?: InputMaybe<Scalars['Bytes']>;
  nftPrice_in?: InputMaybe<Array<Scalars['Bytes']>>;
  nftPrice_not_in?: InputMaybe<Array<Scalars['Bytes']>>;
  nftPrice_contains?: InputMaybe<Scalars['Bytes']>;
  nftPrice_not_contains?: InputMaybe<Scalars['Bytes']>;
  paymentToken?: InputMaybe<Scalars['BigInt']>;
  paymentToken_not?: InputMaybe<Scalars['BigInt']>;
  paymentToken_gt?: InputMaybe<Scalars['BigInt']>;
  paymentToken_lt?: InputMaybe<Scalars['BigInt']>;
  paymentToken_gte?: InputMaybe<Scalars['BigInt']>;
  paymentToken_lte?: InputMaybe<Scalars['BigInt']>;
  paymentToken_in?: InputMaybe<Array<Scalars['BigInt']>>;
  paymentToken_not_in?: InputMaybe<Array<Scalars['BigInt']>>;
  lentAmount?: InputMaybe<Scalars['BigInt']>;
  lentAmount_not?: InputMaybe<Scalars['BigInt']>;
  lentAmount_gt?: InputMaybe<Scalars['BigInt']>;
  lentAmount_lt?: InputMaybe<Scalars['BigInt']>;
  lentAmount_gte?: InputMaybe<Scalars['BigInt']>;
  lentAmount_lte?: InputMaybe<Scalars['BigInt']>;
  lentAmount_in?: InputMaybe<Array<Scalars['BigInt']>>;
  lentAmount_not_in?: InputMaybe<Array<Scalars['BigInt']>>;
  isERC721?: InputMaybe<Scalars['Boolean']>;
  isERC721_not?: InputMaybe<Scalars['Boolean']>;
  isERC721_in?: InputMaybe<Array<Scalars['Boolean']>>;
  isERC721_not_in?: InputMaybe<Array<Scalars['Boolean']>>;
  lentAt?: InputMaybe<Scalars['BigInt']>;
  lentAt_not?: InputMaybe<Scalars['BigInt']>;
  lentAt_gt?: InputMaybe<Scalars['BigInt']>;
  lentAt_lt?: InputMaybe<Scalars['BigInt']>;
  lentAt_gte?: InputMaybe<Scalars['BigInt']>;
  lentAt_lte?: InputMaybe<Scalars['BigInt']>;
  lentAt_in?: InputMaybe<Array<Scalars['BigInt']>>;
  lentAt_not_in?: InputMaybe<Array<Scalars['BigInt']>>;
  renting?: InputMaybe<Scalars['String']>;
  renting_not?: InputMaybe<Scalars['String']>;
  renting_gt?: InputMaybe<Scalars['String']>;
  renting_lt?: InputMaybe<Scalars['String']>;
  renting_gte?: InputMaybe<Scalars['String']>;
  renting_lte?: InputMaybe<Scalars['String']>;
  renting_in?: InputMaybe<Array<Scalars['String']>>;
  renting_not_in?: InputMaybe<Array<Scalars['String']>>;
  renting_contains?: InputMaybe<Scalars['String']>;
  renting_contains_nocase?: InputMaybe<Scalars['String']>;
  renting_not_contains?: InputMaybe<Scalars['String']>;
  renting_not_contains_nocase?: InputMaybe<Scalars['String']>;
  renting_starts_with?: InputMaybe<Scalars['String']>;
  renting_starts_with_nocase?: InputMaybe<Scalars['String']>;
  renting_not_starts_with?: InputMaybe<Scalars['String']>;
  renting_not_starts_with_nocase?: InputMaybe<Scalars['String']>;
  renting_ends_with?: InputMaybe<Scalars['String']>;
  renting_ends_with_nocase?: InputMaybe<Scalars['String']>;
  renting_not_ends_with?: InputMaybe<Scalars['String']>;
  renting_not_ends_with_nocase?: InputMaybe<Scalars['String']>;
  renting_?: InputMaybe<Renting_filter>;
  collateralClaimed?: InputMaybe<Scalars['Boolean']>;
  collateralClaimed_not?: InputMaybe<Scalars['Boolean']>;
  collateralClaimed_in?: InputMaybe<Array<Scalars['Boolean']>>;
  collateralClaimed_not_in?: InputMaybe<Array<Scalars['Boolean']>>;
  nft?: InputMaybe<Scalars['String']>;
  nft_not?: InputMaybe<Scalars['String']>;
  nft_gt?: InputMaybe<Scalars['String']>;
  nft_lt?: InputMaybe<Scalars['String']>;
  nft_gte?: InputMaybe<Scalars['String']>;
  nft_lte?: InputMaybe<Scalars['String']>;
  nft_in?: InputMaybe<Array<Scalars['String']>>;
  nft_not_in?: InputMaybe<Array<Scalars['String']>>;
  nft_contains?: InputMaybe<Scalars['String']>;
  nft_contains_nocase?: InputMaybe<Scalars['String']>;
  nft_not_contains?: InputMaybe<Scalars['String']>;
  nft_not_contains_nocase?: InputMaybe<Scalars['String']>;
  nft_starts_with?: InputMaybe<Scalars['String']>;
  nft_starts_with_nocase?: InputMaybe<Scalars['String']>;
  nft_not_starts_with?: InputMaybe<Scalars['String']>;
  nft_not_starts_with_nocase?: InputMaybe<Scalars['String']>;
  nft_ends_with?: InputMaybe<Scalars['String']>;
  nft_ends_with_nocase?: InputMaybe<Scalars['String']>;
  nft_not_ends_with?: InputMaybe<Scalars['String']>;
  nft_not_ends_with_nocase?: InputMaybe<Scalars['String']>;
  nft_?: InputMaybe<Nft_filter>;
  lenderUser?: InputMaybe<Scalars['String']>;
  lenderUser_not?: InputMaybe<Scalars['String']>;
  lenderUser_gt?: InputMaybe<Scalars['String']>;
  lenderUser_lt?: InputMaybe<Scalars['String']>;
  lenderUser_gte?: InputMaybe<Scalars['String']>;
  lenderUser_lte?: InputMaybe<Scalars['String']>;
  lenderUser_in?: InputMaybe<Array<Scalars['String']>>;
  lenderUser_not_in?: InputMaybe<Array<Scalars['String']>>;
  lenderUser_contains?: InputMaybe<Scalars['String']>;
  lenderUser_contains_nocase?: InputMaybe<Scalars['String']>;
  lenderUser_not_contains?: InputMaybe<Scalars['String']>;
  lenderUser_not_contains_nocase?: InputMaybe<Scalars['String']>;
  lenderUser_starts_with?: InputMaybe<Scalars['String']>;
  lenderUser_starts_with_nocase?: InputMaybe<Scalars['String']>;
  lenderUser_not_starts_with?: InputMaybe<Scalars['String']>;
  lenderUser_not_starts_with_nocase?: InputMaybe<Scalars['String']>;
  lenderUser_ends_with?: InputMaybe<Scalars['String']>;
  lenderUser_ends_with_nocase?: InputMaybe<Scalars['String']>;
  lenderUser_not_ends_with?: InputMaybe<Scalars['String']>;
  lenderUser_not_ends_with_nocase?: InputMaybe<Scalars['String']>;
  lenderUser_?: InputMaybe<User_filter>;
  /** Filter for the block changed event. */
  _change_block?: InputMaybe<BlockChangedFilter>;
};

export type Lending_orderBy =
  | 'id'
  | 'cursor'
  | 'nftAddress'
  | 'tokenId'
  | 'lenderAddress'
  | 'maxRentDuration'
  | 'dailyRentPrice'
  | 'nftPrice'
  | 'paymentToken'
  | 'lentAmount'
  | 'isERC721'
  | 'lentAt'
  | 'renting'
  | 'collateralClaimed'
  | 'nft'
  | 'lenderUser';

export type Nft = {
  /** id is nftAddress::tokenId::lentAmount */
  id: Scalars['ID'];
  /** each Lending and Renting in the arrays here will have the SAME nftAddress and tokenId!!!!!! As per the id of this entity */
  lending: Array<Lending>;
  renting?: Maybe<Array<Renting>>;
};


export type NftlendingArgs = {
  skip?: InputMaybe<Scalars['Int']>;
  first?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Lending_orderBy>;
  orderDirection?: InputMaybe<OrderDirection>;
  where?: InputMaybe<Lending_filter>;
};


export type NftrentingArgs = {
  skip?: InputMaybe<Scalars['Int']>;
  first?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Renting_orderBy>;
  orderDirection?: InputMaybe<OrderDirection>;
  where?: InputMaybe<Renting_filter>;
};

export type Nft_filter = {
  id?: InputMaybe<Scalars['ID']>;
  id_not?: InputMaybe<Scalars['ID']>;
  id_gt?: InputMaybe<Scalars['ID']>;
  id_lt?: InputMaybe<Scalars['ID']>;
  id_gte?: InputMaybe<Scalars['ID']>;
  id_lte?: InputMaybe<Scalars['ID']>;
  id_in?: InputMaybe<Array<Scalars['ID']>>;
  id_not_in?: InputMaybe<Array<Scalars['ID']>>;
  lending_?: InputMaybe<Lending_filter>;
  renting_?: InputMaybe<Renting_filter>;
  /** Filter for the block changed event. */
  _change_block?: InputMaybe<BlockChangedFilter>;
};

export type Nft_orderBy =
  | 'id'
  | 'lending'
  | 'renting';

/** Defines the order direction, either ascending or descending */
export type OrderDirection =
  | 'asc'
  | 'desc';

export type Query = {
  lending?: Maybe<Lending>;
  lendings: Array<Lending>;
  renting?: Maybe<Renting>;
  rentings: Array<Renting>;
  nft?: Maybe<Nft>;
  nfts: Array<Nft>;
  user?: Maybe<User>;
  users: Array<User>;
  lendingRentingCount?: Maybe<LendingRentingCount>;
  lendingRentingCounts: Array<LendingRentingCount>;
  counter?: Maybe<Counter>;
  counters: Array<Counter>;
  /** Access to subgraph metadata */
  _meta?: Maybe<_Meta_>;
};


export type QuerylendingArgs = {
  id: Scalars['ID'];
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};


export type QuerylendingsArgs = {
  skip?: InputMaybe<Scalars['Int']>;
  first?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Lending_orderBy>;
  orderDirection?: InputMaybe<OrderDirection>;
  where?: InputMaybe<Lending_filter>;
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};


export type QueryrentingArgs = {
  id: Scalars['ID'];
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};


export type QueryrentingsArgs = {
  skip?: InputMaybe<Scalars['Int']>;
  first?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Renting_orderBy>;
  orderDirection?: InputMaybe<OrderDirection>;
  where?: InputMaybe<Renting_filter>;
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};


export type QuerynftArgs = {
  id: Scalars['ID'];
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};


export type QuerynftsArgs = {
  skip?: InputMaybe<Scalars['Int']>;
  first?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Nft_orderBy>;
  orderDirection?: InputMaybe<OrderDirection>;
  where?: InputMaybe<Nft_filter>;
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};


export type QueryuserArgs = {
  id: Scalars['ID'];
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};


export type QueryusersArgs = {
  skip?: InputMaybe<Scalars['Int']>;
  first?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<User_orderBy>;
  orderDirection?: InputMaybe<OrderDirection>;
  where?: InputMaybe<User_filter>;
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};


export type QuerylendingRentingCountArgs = {
  id: Scalars['ID'];
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};


export type QuerylendingRentingCountsArgs = {
  skip?: InputMaybe<Scalars['Int']>;
  first?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<LendingRentingCount_orderBy>;
  orderDirection?: InputMaybe<OrderDirection>;
  where?: InputMaybe<LendingRentingCount_filter>;
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};


export type QuerycounterArgs = {
  id: Scalars['ID'];
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};


export type QuerycountersArgs = {
  skip?: InputMaybe<Scalars['Int']>;
  first?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Counter_orderBy>;
  orderDirection?: InputMaybe<OrderDirection>;
  where?: InputMaybe<Counter_filter>;
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};


export type Query_metaArgs = {
  block?: InputMaybe<Block_height>;
};

export type Renting = {
  id: Scalars['ID'];
  cursor: Scalars['Int'];
  renterAddress: Scalars['Bytes'];
  rentDuration: Scalars['BigInt'];
  rentedAt: Scalars['BigInt'];
  lending: Lending;
  /** id := nftAddress::tokenId::lentAmount */
  nft: Nft;
  renterUser: User;
};

export type Renting_filter = {
  id?: InputMaybe<Scalars['ID']>;
  id_not?: InputMaybe<Scalars['ID']>;
  id_gt?: InputMaybe<Scalars['ID']>;
  id_lt?: InputMaybe<Scalars['ID']>;
  id_gte?: InputMaybe<Scalars['ID']>;
  id_lte?: InputMaybe<Scalars['ID']>;
  id_in?: InputMaybe<Array<Scalars['ID']>>;
  id_not_in?: InputMaybe<Array<Scalars['ID']>>;
  cursor?: InputMaybe<Scalars['Int']>;
  cursor_not?: InputMaybe<Scalars['Int']>;
  cursor_gt?: InputMaybe<Scalars['Int']>;
  cursor_lt?: InputMaybe<Scalars['Int']>;
  cursor_gte?: InputMaybe<Scalars['Int']>;
  cursor_lte?: InputMaybe<Scalars['Int']>;
  cursor_in?: InputMaybe<Array<Scalars['Int']>>;
  cursor_not_in?: InputMaybe<Array<Scalars['Int']>>;
  renterAddress?: InputMaybe<Scalars['Bytes']>;
  renterAddress_not?: InputMaybe<Scalars['Bytes']>;
  renterAddress_in?: InputMaybe<Array<Scalars['Bytes']>>;
  renterAddress_not_in?: InputMaybe<Array<Scalars['Bytes']>>;
  renterAddress_contains?: InputMaybe<Scalars['Bytes']>;
  renterAddress_not_contains?: InputMaybe<Scalars['Bytes']>;
  rentDuration?: InputMaybe<Scalars['BigInt']>;
  rentDuration_not?: InputMaybe<Scalars['BigInt']>;
  rentDuration_gt?: InputMaybe<Scalars['BigInt']>;
  rentDuration_lt?: InputMaybe<Scalars['BigInt']>;
  rentDuration_gte?: InputMaybe<Scalars['BigInt']>;
  rentDuration_lte?: InputMaybe<Scalars['BigInt']>;
  rentDuration_in?: InputMaybe<Array<Scalars['BigInt']>>;
  rentDuration_not_in?: InputMaybe<Array<Scalars['BigInt']>>;
  rentedAt?: InputMaybe<Scalars['BigInt']>;
  rentedAt_not?: InputMaybe<Scalars['BigInt']>;
  rentedAt_gt?: InputMaybe<Scalars['BigInt']>;
  rentedAt_lt?: InputMaybe<Scalars['BigInt']>;
  rentedAt_gte?: InputMaybe<Scalars['BigInt']>;
  rentedAt_lte?: InputMaybe<Scalars['BigInt']>;
  rentedAt_in?: InputMaybe<Array<Scalars['BigInt']>>;
  rentedAt_not_in?: InputMaybe<Array<Scalars['BigInt']>>;
  lending?: InputMaybe<Scalars['String']>;
  lending_not?: InputMaybe<Scalars['String']>;
  lending_gt?: InputMaybe<Scalars['String']>;
  lending_lt?: InputMaybe<Scalars['String']>;
  lending_gte?: InputMaybe<Scalars['String']>;
  lending_lte?: InputMaybe<Scalars['String']>;
  lending_in?: InputMaybe<Array<Scalars['String']>>;
  lending_not_in?: InputMaybe<Array<Scalars['String']>>;
  lending_contains?: InputMaybe<Scalars['String']>;
  lending_contains_nocase?: InputMaybe<Scalars['String']>;
  lending_not_contains?: InputMaybe<Scalars['String']>;
  lending_not_contains_nocase?: InputMaybe<Scalars['String']>;
  lending_starts_with?: InputMaybe<Scalars['String']>;
  lending_starts_with_nocase?: InputMaybe<Scalars['String']>;
  lending_not_starts_with?: InputMaybe<Scalars['String']>;
  lending_not_starts_with_nocase?: InputMaybe<Scalars['String']>;
  lending_ends_with?: InputMaybe<Scalars['String']>;
  lending_ends_with_nocase?: InputMaybe<Scalars['String']>;
  lending_not_ends_with?: InputMaybe<Scalars['String']>;
  lending_not_ends_with_nocase?: InputMaybe<Scalars['String']>;
  lending_?: InputMaybe<Lending_filter>;
  nft?: InputMaybe<Scalars['String']>;
  nft_not?: InputMaybe<Scalars['String']>;
  nft_gt?: InputMaybe<Scalars['String']>;
  nft_lt?: InputMaybe<Scalars['String']>;
  nft_gte?: InputMaybe<Scalars['String']>;
  nft_lte?: InputMaybe<Scalars['String']>;
  nft_in?: InputMaybe<Array<Scalars['String']>>;
  nft_not_in?: InputMaybe<Array<Scalars['String']>>;
  nft_contains?: InputMaybe<Scalars['String']>;
  nft_contains_nocase?: InputMaybe<Scalars['String']>;
  nft_not_contains?: InputMaybe<Scalars['String']>;
  nft_not_contains_nocase?: InputMaybe<Scalars['String']>;
  nft_starts_with?: InputMaybe<Scalars['String']>;
  nft_starts_with_nocase?: InputMaybe<Scalars['String']>;
  nft_not_starts_with?: InputMaybe<Scalars['String']>;
  nft_not_starts_with_nocase?: InputMaybe<Scalars['String']>;
  nft_ends_with?: InputMaybe<Scalars['String']>;
  nft_ends_with_nocase?: InputMaybe<Scalars['String']>;
  nft_not_ends_with?: InputMaybe<Scalars['String']>;
  nft_not_ends_with_nocase?: InputMaybe<Scalars['String']>;
  nft_?: InputMaybe<Nft_filter>;
  renterUser?: InputMaybe<Scalars['String']>;
  renterUser_not?: InputMaybe<Scalars['String']>;
  renterUser_gt?: InputMaybe<Scalars['String']>;
  renterUser_lt?: InputMaybe<Scalars['String']>;
  renterUser_gte?: InputMaybe<Scalars['String']>;
  renterUser_lte?: InputMaybe<Scalars['String']>;
  renterUser_in?: InputMaybe<Array<Scalars['String']>>;
  renterUser_not_in?: InputMaybe<Array<Scalars['String']>>;
  renterUser_contains?: InputMaybe<Scalars['String']>;
  renterUser_contains_nocase?: InputMaybe<Scalars['String']>;
  renterUser_not_contains?: InputMaybe<Scalars['String']>;
  renterUser_not_contains_nocase?: InputMaybe<Scalars['String']>;
  renterUser_starts_with?: InputMaybe<Scalars['String']>;
  renterUser_starts_with_nocase?: InputMaybe<Scalars['String']>;
  renterUser_not_starts_with?: InputMaybe<Scalars['String']>;
  renterUser_not_starts_with_nocase?: InputMaybe<Scalars['String']>;
  renterUser_ends_with?: InputMaybe<Scalars['String']>;
  renterUser_ends_with_nocase?: InputMaybe<Scalars['String']>;
  renterUser_not_ends_with?: InputMaybe<Scalars['String']>;
  renterUser_not_ends_with_nocase?: InputMaybe<Scalars['String']>;
  renterUser_?: InputMaybe<User_filter>;
  /** Filter for the block changed event. */
  _change_block?: InputMaybe<BlockChangedFilter>;
};

export type Renting_orderBy =
  | 'id'
  | 'cursor'
  | 'renterAddress'
  | 'rentDuration'
  | 'rentedAt'
  | 'lending'
  | 'nft'
  | 'renterUser';

export type Subscription = {
  lending?: Maybe<Lending>;
  lendings: Array<Lending>;
  renting?: Maybe<Renting>;
  rentings: Array<Renting>;
  nft?: Maybe<Nft>;
  nfts: Array<Nft>;
  user?: Maybe<User>;
  users: Array<User>;
  lendingRentingCount?: Maybe<LendingRentingCount>;
  lendingRentingCounts: Array<LendingRentingCount>;
  counter?: Maybe<Counter>;
  counters: Array<Counter>;
  /** Access to subgraph metadata */
  _meta?: Maybe<_Meta_>;
};


export type SubscriptionlendingArgs = {
  id: Scalars['ID'];
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};


export type SubscriptionlendingsArgs = {
  skip?: InputMaybe<Scalars['Int']>;
  first?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Lending_orderBy>;
  orderDirection?: InputMaybe<OrderDirection>;
  where?: InputMaybe<Lending_filter>;
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};


export type SubscriptionrentingArgs = {
  id: Scalars['ID'];
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};


export type SubscriptionrentingsArgs = {
  skip?: InputMaybe<Scalars['Int']>;
  first?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Renting_orderBy>;
  orderDirection?: InputMaybe<OrderDirection>;
  where?: InputMaybe<Renting_filter>;
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};


export type SubscriptionnftArgs = {
  id: Scalars['ID'];
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};


export type SubscriptionnftsArgs = {
  skip?: InputMaybe<Scalars['Int']>;
  first?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Nft_orderBy>;
  orderDirection?: InputMaybe<OrderDirection>;
  where?: InputMaybe<Nft_filter>;
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};


export type SubscriptionuserArgs = {
  id: Scalars['ID'];
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};


export type SubscriptionusersArgs = {
  skip?: InputMaybe<Scalars['Int']>;
  first?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<User_orderBy>;
  orderDirection?: InputMaybe<OrderDirection>;
  where?: InputMaybe<User_filter>;
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};


export type SubscriptionlendingRentingCountArgs = {
  id: Scalars['ID'];
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};


export type SubscriptionlendingRentingCountsArgs = {
  skip?: InputMaybe<Scalars['Int']>;
  first?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<LendingRentingCount_orderBy>;
  orderDirection?: InputMaybe<OrderDirection>;
  where?: InputMaybe<LendingRentingCount_filter>;
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};


export type SubscriptioncounterArgs = {
  id: Scalars['ID'];
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};


export type SubscriptioncountersArgs = {
  skip?: InputMaybe<Scalars['Int']>;
  first?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Counter_orderBy>;
  orderDirection?: InputMaybe<OrderDirection>;
  where?: InputMaybe<Counter_filter>;
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};


export type Subscription_metaArgs = {
  block?: InputMaybe<Block_height>;
};

export type User = {
  /** id here is user's Ethereum address */
  id: Scalars['ID'];
  cursor: Scalars['Int'];
  /** each Lending and Renting in the arrays here will have DIFFERENT nftAddress and tokenId */
  lending?: Maybe<Array<Lending>>;
  renting?: Maybe<Array<Renting>>;
};


export type UserlendingArgs = {
  skip?: InputMaybe<Scalars['Int']>;
  first?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Lending_orderBy>;
  orderDirection?: InputMaybe<OrderDirection>;
  where?: InputMaybe<Lending_filter>;
};


export type UserrentingArgs = {
  skip?: InputMaybe<Scalars['Int']>;
  first?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Renting_orderBy>;
  orderDirection?: InputMaybe<OrderDirection>;
  where?: InputMaybe<Renting_filter>;
};

export type User_filter = {
  id?: InputMaybe<Scalars['ID']>;
  id_not?: InputMaybe<Scalars['ID']>;
  id_gt?: InputMaybe<Scalars['ID']>;
  id_lt?: InputMaybe<Scalars['ID']>;
  id_gte?: InputMaybe<Scalars['ID']>;
  id_lte?: InputMaybe<Scalars['ID']>;
  id_in?: InputMaybe<Array<Scalars['ID']>>;
  id_not_in?: InputMaybe<Array<Scalars['ID']>>;
  cursor?: InputMaybe<Scalars['Int']>;
  cursor_not?: InputMaybe<Scalars['Int']>;
  cursor_gt?: InputMaybe<Scalars['Int']>;
  cursor_lt?: InputMaybe<Scalars['Int']>;
  cursor_gte?: InputMaybe<Scalars['Int']>;
  cursor_lte?: InputMaybe<Scalars['Int']>;
  cursor_in?: InputMaybe<Array<Scalars['Int']>>;
  cursor_not_in?: InputMaybe<Array<Scalars['Int']>>;
  lending_?: InputMaybe<Lending_filter>;
  renting_?: InputMaybe<Renting_filter>;
  /** Filter for the block changed event. */
  _change_block?: InputMaybe<BlockChangedFilter>;
};

export type User_orderBy =
  | 'id'
  | 'cursor'
  | 'lending'
  | 'renting';

export type _Block_ = {
  /** The hash of the block */
  hash?: Maybe<Scalars['Bytes']>;
  /** The block number */
  number: Scalars['Int'];
  /** Integer representation of the timestamp stored in blocks for the chain */
  timestamp?: Maybe<Scalars['Int']>;
};

/** The type for the top-level _meta field */
export type _Meta_ = {
  /**
   * Information about a specific subgraph block. The hash of the block
   * will be null if the _meta field has a block constraint that asks for
   * a block number. It will be filled if the _meta field has no block constraint
   * and therefore asks for the latest  block
   *
   */
  block: _Block_;
  /** The deployment ID */
  deployment: Scalars['String'];
  /** If `true`, the subgraph encountered indexing errors at some past block */
  hasIndexingErrors: Scalars['Boolean'];
};

export type _SubgraphErrorPolicy_ =
  /** Data will be returned even if the subgraph has indexing errors */
  | 'allow'
  /** If the subgraph has indexing errors, data will be omitted. The default. */
  | 'deny';

export type WithIndex<TObject> = TObject & Record<string, any>;
export type ResolversObject<TObject> = WithIndex<TObject>;

export type ResolverTypeWrapper<T> = Promise<T> | T;


export type ResolverWithResolve<TResult, TParent, TContext, TArgs> = {
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};

export type LegacyStitchingResolver<TResult, TParent, TContext, TArgs> = {
  fragment: string;
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};

export type NewStitchingResolver<TResult, TParent, TContext, TArgs> = {
  selectionSet: string | ((fieldNode: FieldNode) => SelectionSetNode);
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};
export type StitchingResolver<TResult, TParent, TContext, TArgs> = LegacyStitchingResolver<TResult, TParent, TContext, TArgs> | NewStitchingResolver<TResult, TParent, TContext, TArgs>;
export type Resolver<TResult, TParent = {}, TContext = {}, TArgs = {}> =
  | ResolverFn<TResult, TParent, TContext, TArgs>
  | ResolverWithResolve<TResult, TParent, TContext, TArgs>
  | StitchingResolver<TResult, TParent, TContext, TArgs>;

export type ResolverFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => Promise<TResult> | TResult;

export type SubscriptionSubscribeFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => AsyncIterable<TResult> | Promise<AsyncIterable<TResult>>;

export type SubscriptionResolveFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

export interface SubscriptionSubscriberObject<TResult, TKey extends string, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<{ [key in TKey]: TResult }, TParent, TContext, TArgs>;
  resolve?: SubscriptionResolveFn<TResult, { [key in TKey]: TResult }, TContext, TArgs>;
}

export interface SubscriptionResolverObject<TResult, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<any, TParent, TContext, TArgs>;
  resolve: SubscriptionResolveFn<TResult, any, TContext, TArgs>;
}

export type SubscriptionObject<TResult, TKey extends string, TParent, TContext, TArgs> =
  | SubscriptionSubscriberObject<TResult, TKey, TParent, TContext, TArgs>
  | SubscriptionResolverObject<TResult, TParent, TContext, TArgs>;

export type SubscriptionResolver<TResult, TKey extends string, TParent = {}, TContext = {}, TArgs = {}> =
  | ((...args: any[]) => SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>)
  | SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>;

export type TypeResolveFn<TTypes, TParent = {}, TContext = {}> = (
  parent: TParent,
  context: TContext,
  info: GraphQLResolveInfo
) => Maybe<TTypes> | Promise<Maybe<TTypes>>;

export type IsTypeOfResolverFn<T = {}, TContext = {}> = (obj: T, context: TContext, info: GraphQLResolveInfo) => boolean | Promise<boolean>;

export type NextResolverFn<T> = () => Promise<T>;

export type DirectiveResolverFn<TResult = {}, TParent = {}, TContext = {}, TArgs = {}> = (
  next: NextResolverFn<TResult>,
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

/** Mapping between all available schema types and the resolvers types */
export type ResolversTypes = ResolversObject<{
  BigDecimal: ResolverTypeWrapper<Scalars['BigDecimal']>;
  BigInt: ResolverTypeWrapper<Scalars['BigInt']>;
  BlockChangedFilter: BlockChangedFilter;
  Block_height: Block_height;
  Boolean: ResolverTypeWrapper<Scalars['Boolean']>;
  Bytes: ResolverTypeWrapper<Scalars['Bytes']>;
  Counter: ResolverTypeWrapper<Counter>;
  Counter_filter: Counter_filter;
  Counter_orderBy: Counter_orderBy;
  Float: ResolverTypeWrapper<Scalars['Float']>;
  ID: ResolverTypeWrapper<Scalars['ID']>;
  Int: ResolverTypeWrapper<Scalars['Int']>;
  Lending: ResolverTypeWrapper<Lending>;
  LendingRentingCount: ResolverTypeWrapper<LendingRentingCount>;
  LendingRentingCount_filter: LendingRentingCount_filter;
  LendingRentingCount_orderBy: LendingRentingCount_orderBy;
  Lending_filter: Lending_filter;
  Lending_orderBy: Lending_orderBy;
  Nft: ResolverTypeWrapper<Nft>;
  Nft_filter: Nft_filter;
  Nft_orderBy: Nft_orderBy;
  OrderDirection: OrderDirection;
  Query: ResolverTypeWrapper<{}>;
  Renting: ResolverTypeWrapper<Renting>;
  Renting_filter: Renting_filter;
  Renting_orderBy: Renting_orderBy;
  String: ResolverTypeWrapper<Scalars['String']>;
  Subscription: ResolverTypeWrapper<{}>;
  User: ResolverTypeWrapper<User>;
  User_filter: User_filter;
  User_orderBy: User_orderBy;
  _Block_: ResolverTypeWrapper<_Block_>;
  _Meta_: ResolverTypeWrapper<_Meta_>;
  _SubgraphErrorPolicy_: _SubgraphErrorPolicy_;
}>;

/** Mapping between all available schema types and the resolvers parents */
export type ResolversParentTypes = ResolversObject<{
  BigDecimal: Scalars['BigDecimal'];
  BigInt: Scalars['BigInt'];
  BlockChangedFilter: BlockChangedFilter;
  Block_height: Block_height;
  Boolean: Scalars['Boolean'];
  Bytes: Scalars['Bytes'];
  Counter: Counter;
  Counter_filter: Counter_filter;
  Float: Scalars['Float'];
  ID: Scalars['ID'];
  Int: Scalars['Int'];
  Lending: Lending;
  LendingRentingCount: LendingRentingCount;
  LendingRentingCount_filter: LendingRentingCount_filter;
  Lending_filter: Lending_filter;
  Nft: Nft;
  Nft_filter: Nft_filter;
  Query: {};
  Renting: Renting;
  Renting_filter: Renting_filter;
  String: Scalars['String'];
  Subscription: {};
  User: User;
  User_filter: User_filter;
  _Block_: _Block_;
  _Meta_: _Meta_;
}>;

export type entityDirectiveArgs = { };

export type entityDirectiveResolver<Result, Parent, ContextType = MeshContext, Args = entityDirectiveArgs> = DirectiveResolverFn<Result, Parent, ContextType, Args>;

export type subgraphIdDirectiveArgs = {
  id: Scalars['String'];
};

export type subgraphIdDirectiveResolver<Result, Parent, ContextType = MeshContext, Args = subgraphIdDirectiveArgs> = DirectiveResolverFn<Result, Parent, ContextType, Args>;

export type derivedFromDirectiveArgs = {
  field: Scalars['String'];
};

export type derivedFromDirectiveResolver<Result, Parent, ContextType = MeshContext, Args = derivedFromDirectiveArgs> = DirectiveResolverFn<Result, Parent, ContextType, Args>;

export interface BigDecimalScalarConfig extends GraphQLScalarTypeConfig<ResolversTypes['BigDecimal'], any> {
  name: 'BigDecimal';
}

export interface BigIntScalarConfig extends GraphQLScalarTypeConfig<ResolversTypes['BigInt'], any> {
  name: 'BigInt';
}

export interface BytesScalarConfig extends GraphQLScalarTypeConfig<ResolversTypes['Bytes'], any> {
  name: 'Bytes';
}

export type CounterResolvers<ContextType = MeshContext, ParentType extends ResolversParentTypes['Counter'] = ResolversParentTypes['Counter']> = ResolversObject<{
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  lending?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  renting?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  user?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type LendingResolvers<ContextType = MeshContext, ParentType extends ResolversParentTypes['Lending'] = ResolversParentTypes['Lending']> = ResolversObject<{
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  cursor?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  nftAddress?: Resolver<ResolversTypes['Bytes'], ParentType, ContextType>;
  tokenId?: Resolver<ResolversTypes['BigInt'], ParentType, ContextType>;
  lenderAddress?: Resolver<ResolversTypes['Bytes'], ParentType, ContextType>;
  maxRentDuration?: Resolver<ResolversTypes['BigInt'], ParentType, ContextType>;
  dailyRentPrice?: Resolver<ResolversTypes['Bytes'], ParentType, ContextType>;
  nftPrice?: Resolver<ResolversTypes['Bytes'], ParentType, ContextType>;
  paymentToken?: Resolver<ResolversTypes['BigInt'], ParentType, ContextType>;
  lentAmount?: Resolver<ResolversTypes['BigInt'], ParentType, ContextType>;
  isERC721?: Resolver<ResolversTypes['Boolean'], ParentType, ContextType>;
  lentAt?: Resolver<ResolversTypes['BigInt'], ParentType, ContextType>;
  renting?: Resolver<Maybe<ResolversTypes['Renting']>, ParentType, ContextType>;
  collateralClaimed?: Resolver<ResolversTypes['Boolean'], ParentType, ContextType>;
  nft?: Resolver<ResolversTypes['Nft'], ParentType, ContextType>;
  lenderUser?: Resolver<ResolversTypes['User'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type LendingRentingCountResolvers<ContextType = MeshContext, ParentType extends ResolversParentTypes['LendingRentingCount'] = ResolversParentTypes['LendingRentingCount']> = ResolversObject<{
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  lending?: Resolver<ResolversTypes['BigInt'], ParentType, ContextType>;
  renting?: Resolver<ResolversTypes['BigInt'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type NftResolvers<ContextType = MeshContext, ParentType extends ResolversParentTypes['Nft'] = ResolversParentTypes['Nft']> = ResolversObject<{
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  lending?: Resolver<Array<ResolversTypes['Lending']>, ParentType, ContextType, RequireFields<NftlendingArgs, 'skip' | 'first'>>;
  renting?: Resolver<Maybe<Array<ResolversTypes['Renting']>>, ParentType, ContextType, RequireFields<NftrentingArgs, 'skip' | 'first'>>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type QueryResolvers<ContextType = MeshContext, ParentType extends ResolversParentTypes['Query'] = ResolversParentTypes['Query']> = ResolversObject<{
  lending?: Resolver<Maybe<ResolversTypes['Lending']>, ParentType, ContextType, RequireFields<QuerylendingArgs, 'id' | 'subgraphError'>>;
  lendings?: Resolver<Array<ResolversTypes['Lending']>, ParentType, ContextType, RequireFields<QuerylendingsArgs, 'skip' | 'first' | 'subgraphError'>>;
  renting?: Resolver<Maybe<ResolversTypes['Renting']>, ParentType, ContextType, RequireFields<QueryrentingArgs, 'id' | 'subgraphError'>>;
  rentings?: Resolver<Array<ResolversTypes['Renting']>, ParentType, ContextType, RequireFields<QueryrentingsArgs, 'skip' | 'first' | 'subgraphError'>>;
  nft?: Resolver<Maybe<ResolversTypes['Nft']>, ParentType, ContextType, RequireFields<QuerynftArgs, 'id' | 'subgraphError'>>;
  nfts?: Resolver<Array<ResolversTypes['Nft']>, ParentType, ContextType, RequireFields<QuerynftsArgs, 'skip' | 'first' | 'subgraphError'>>;
  user?: Resolver<Maybe<ResolversTypes['User']>, ParentType, ContextType, RequireFields<QueryuserArgs, 'id' | 'subgraphError'>>;
  users?: Resolver<Array<ResolversTypes['User']>, ParentType, ContextType, RequireFields<QueryusersArgs, 'skip' | 'first' | 'subgraphError'>>;
  lendingRentingCount?: Resolver<Maybe<ResolversTypes['LendingRentingCount']>, ParentType, ContextType, RequireFields<QuerylendingRentingCountArgs, 'id' | 'subgraphError'>>;
  lendingRentingCounts?: Resolver<Array<ResolversTypes['LendingRentingCount']>, ParentType, ContextType, RequireFields<QuerylendingRentingCountsArgs, 'skip' | 'first' | 'subgraphError'>>;
  counter?: Resolver<Maybe<ResolversTypes['Counter']>, ParentType, ContextType, RequireFields<QuerycounterArgs, 'id' | 'subgraphError'>>;
  counters?: Resolver<Array<ResolversTypes['Counter']>, ParentType, ContextType, RequireFields<QuerycountersArgs, 'skip' | 'first' | 'subgraphError'>>;
  _meta?: Resolver<Maybe<ResolversTypes['_Meta_']>, ParentType, ContextType, Partial<Query_metaArgs>>;
}>;

export type RentingResolvers<ContextType = MeshContext, ParentType extends ResolversParentTypes['Renting'] = ResolversParentTypes['Renting']> = ResolversObject<{
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  cursor?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  renterAddress?: Resolver<ResolversTypes['Bytes'], ParentType, ContextType>;
  rentDuration?: Resolver<ResolversTypes['BigInt'], ParentType, ContextType>;
  rentedAt?: Resolver<ResolversTypes['BigInt'], ParentType, ContextType>;
  lending?: Resolver<ResolversTypes['Lending'], ParentType, ContextType>;
  nft?: Resolver<ResolversTypes['Nft'], ParentType, ContextType>;
  renterUser?: Resolver<ResolversTypes['User'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type SubscriptionResolvers<ContextType = MeshContext, ParentType extends ResolversParentTypes['Subscription'] = ResolversParentTypes['Subscription']> = ResolversObject<{
  lending?: SubscriptionResolver<Maybe<ResolversTypes['Lending']>, "lending", ParentType, ContextType, RequireFields<SubscriptionlendingArgs, 'id' | 'subgraphError'>>;
  lendings?: SubscriptionResolver<Array<ResolversTypes['Lending']>, "lendings", ParentType, ContextType, RequireFields<SubscriptionlendingsArgs, 'skip' | 'first' | 'subgraphError'>>;
  renting?: SubscriptionResolver<Maybe<ResolversTypes['Renting']>, "renting", ParentType, ContextType, RequireFields<SubscriptionrentingArgs, 'id' | 'subgraphError'>>;
  rentings?: SubscriptionResolver<Array<ResolversTypes['Renting']>, "rentings", ParentType, ContextType, RequireFields<SubscriptionrentingsArgs, 'skip' | 'first' | 'subgraphError'>>;
  nft?: SubscriptionResolver<Maybe<ResolversTypes['Nft']>, "nft", ParentType, ContextType, RequireFields<SubscriptionnftArgs, 'id' | 'subgraphError'>>;
  nfts?: SubscriptionResolver<Array<ResolversTypes['Nft']>, "nfts", ParentType, ContextType, RequireFields<SubscriptionnftsArgs, 'skip' | 'first' | 'subgraphError'>>;
  user?: SubscriptionResolver<Maybe<ResolversTypes['User']>, "user", ParentType, ContextType, RequireFields<SubscriptionuserArgs, 'id' | 'subgraphError'>>;
  users?: SubscriptionResolver<Array<ResolversTypes['User']>, "users", ParentType, ContextType, RequireFields<SubscriptionusersArgs, 'skip' | 'first' | 'subgraphError'>>;
  lendingRentingCount?: SubscriptionResolver<Maybe<ResolversTypes['LendingRentingCount']>, "lendingRentingCount", ParentType, ContextType, RequireFields<SubscriptionlendingRentingCountArgs, 'id' | 'subgraphError'>>;
  lendingRentingCounts?: SubscriptionResolver<Array<ResolversTypes['LendingRentingCount']>, "lendingRentingCounts", ParentType, ContextType, RequireFields<SubscriptionlendingRentingCountsArgs, 'skip' | 'first' | 'subgraphError'>>;
  counter?: SubscriptionResolver<Maybe<ResolversTypes['Counter']>, "counter", ParentType, ContextType, RequireFields<SubscriptioncounterArgs, 'id' | 'subgraphError'>>;
  counters?: SubscriptionResolver<Array<ResolversTypes['Counter']>, "counters", ParentType, ContextType, RequireFields<SubscriptioncountersArgs, 'skip' | 'first' | 'subgraphError'>>;
  _meta?: SubscriptionResolver<Maybe<ResolversTypes['_Meta_']>, "_meta", ParentType, ContextType, Partial<Subscription_metaArgs>>;
}>;

export type UserResolvers<ContextType = MeshContext, ParentType extends ResolversParentTypes['User'] = ResolversParentTypes['User']> = ResolversObject<{
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  cursor?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  lending?: Resolver<Maybe<Array<ResolversTypes['Lending']>>, ParentType, ContextType, RequireFields<UserlendingArgs, 'skip' | 'first'>>;
  renting?: Resolver<Maybe<Array<ResolversTypes['Renting']>>, ParentType, ContextType, RequireFields<UserrentingArgs, 'skip' | 'first'>>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type _Block_Resolvers<ContextType = MeshContext, ParentType extends ResolversParentTypes['_Block_'] = ResolversParentTypes['_Block_']> = ResolversObject<{
  hash?: Resolver<Maybe<ResolversTypes['Bytes']>, ParentType, ContextType>;
  number?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  timestamp?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type _Meta_Resolvers<ContextType = MeshContext, ParentType extends ResolversParentTypes['_Meta_'] = ResolversParentTypes['_Meta_']> = ResolversObject<{
  block?: Resolver<ResolversTypes['_Block_'], ParentType, ContextType>;
  deployment?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  hasIndexingErrors?: Resolver<ResolversTypes['Boolean'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type Resolvers<ContextType = MeshContext> = ResolversObject<{
  BigDecimal?: GraphQLScalarType;
  BigInt?: GraphQLScalarType;
  Bytes?: GraphQLScalarType;
  Counter?: CounterResolvers<ContextType>;
  Lending?: LendingResolvers<ContextType>;
  LendingRentingCount?: LendingRentingCountResolvers<ContextType>;
  Nft?: NftResolvers<ContextType>;
  Query?: QueryResolvers<ContextType>;
  Renting?: RentingResolvers<ContextType>;
  Subscription?: SubscriptionResolvers<ContextType>;
  User?: UserResolvers<ContextType>;
  _Block_?: _Block_Resolvers<ContextType>;
  _Meta_?: _Meta_Resolvers<ContextType>;
}>;

export type DirectiveResolvers<ContextType = MeshContext> = ResolversObject<{
  entity?: entityDirectiveResolver<any, any, ContextType>;
  subgraphId?: subgraphIdDirectiveResolver<any, any, ContextType>;
  derivedFrom?: derivedFromDirectiveResolver<any, any, ContextType>;
}>;

export type MeshContext = RenftTypes.Context & BaseMeshContext;


import { fileURLToPath } from '@graphql-mesh/utils';
const baseDir = pathModule.join(pathModule.dirname(fileURLToPath(import.meta.url)), '..');

const importFn: ImportFn = <T>(moduleId: string) => {
  const relativeModuleId = (pathModule.isAbsolute(moduleId) ? pathModule.relative(baseDir, moduleId) : moduleId).split('\\').join('/').replace(baseDir + '/', '');
  switch(relativeModuleId) {
    case ".graphclient/sources/renft/introspectionSchema":
      return import("./sources/renft/introspectionSchema") as T;
    
    default:
      return Promise.reject(new Error(`Cannot find module '${relativeModuleId}'.`));
  }
};

const rootStore = new MeshStore('.graphclient', new FsStoreStorageAdapter({
  cwd: baseDir,
  importFn,
  fileType: "ts",
}), {
  readonly: true,
  validate: false
});

export const rawServeConfig: YamlConfig.Config['serve'] = undefined as any
export async function getMeshOptions(): Promise<GetMeshOptions> {
const pubsub = new PubSub();
const sourcesStore = rootStore.child('sources');
const logger = new DefaultLogger("GraphClient");
const cache = new (MeshCache as any)({
      ...({} as any),
      importFn,
      store: rootStore.child('cache'),
      pubsub,
      logger,
    } as any)

const sources: MeshResolvedSource[] = [];
const transforms: MeshTransform[] = [];
const additionalEnvelopPlugins: MeshPlugin<any>[] = [];
const renftTransforms = [];
const additionalTypeDefs = [] as any[];
const renftHandler = new GraphqlHandler({
              name: "renft",
              config: {"endpoint":"https://gateway.thegraph.com/api/4df7ab6dd78a5ae739ee409cc524bdea/subgraphs/id/BEkzgsGPhih7VE6aVwUL4h7EZyXJjZYn16T9PE5XCmou"},
              baseDir,
              cache,
              pubsub,
              store: sourcesStore.child("renft"),
              logger: logger.child("renft"),
              importFn,
            });
sources[0] = {
          name: 'renft',
          handler: renftHandler,
          transforms: renftTransforms
        }
const additionalResolvers = [] as any[]
const merger = new(BareMerger as any)({
        cache,
        pubsub,
        logger: logger.child('bareMerger'),
        store: rootStore.child('bareMerger')
      })

  return {
    sources,
    transforms,
    additionalTypeDefs,
    additionalResolvers,
    cache,
    pubsub,
    merger,
    logger,
    additionalEnvelopPlugins,
    get documents() {
      return [
      {
        document: LendingsQueryDocument,
        get rawSDL() {
          return printWithCache(LendingsQueryDocument);
        },
        location: 'LendingsQueryDocument.graphql'
      }
    ];
    },
    fetchFn,
  };
}

export function createBuiltMeshHTTPHandler(): MeshHTTPHandler<MeshContext> {
  return createMeshHTTPHandler<MeshContext>({
    baseDir,
    getBuiltMesh: getBuiltGraphClient,
    rawServeConfig: undefined,
  })
}


let meshInstance$: Promise<MeshInstance> | undefined;

export function getBuiltGraphClient(): Promise<MeshInstance> {
  if (meshInstance$ == null) {
    meshInstance$ = getMeshOptions().then(meshOptions => getMesh(meshOptions)).then(mesh => {
      const id = mesh.pubsub.subscribe('destroy', () => {
        meshInstance$ = undefined;
        mesh.pubsub.unsubscribe(id);
      });
      return mesh;
    });
  }
  return meshInstance$;
}

export const execute: ExecuteMeshFn = (...args) => getBuiltGraphClient().then(({ execute }) => execute(...args));

export const subscribe: SubscribeMeshFn = (...args) => getBuiltGraphClient().then(({ subscribe }) => subscribe(...args));
export function getBuiltGraphSDK<TGlobalContext = any, TOperationContext = any>(globalContext?: TGlobalContext) {
  const sdkRequester$ = getBuiltGraphClient().then(({ sdkRequesterFactory }) => sdkRequesterFactory(globalContext));
  return getSdk<TOperationContext, TGlobalContext>((...args) => sdkRequester$.then(sdkRequester => sdkRequester(...args)));
}
export type LendingsQueryQueryVariables = Exact<{
  first?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
}>;


export type LendingsQueryQuery = { lendings: Array<Pick<Lending, 'id' | 'nftAddress' | 'tokenId' | 'nftPrice' | 'dailyRentPrice' | 'isERC721'>> };


export const LendingsQueryDocument = gql`
    query LendingsQuery($first: Int, $skip: Int) {
  lendings(first: $first, skip: $skip) {
    id
    nftAddress
    tokenId
    nftPrice
    dailyRentPrice
    isERC721
  }
}
    ` as unknown as DocumentNode<LendingsQueryQuery, LendingsQueryQueryVariables>;


export type Requester<C = {}, E = unknown> = <R, V>(doc: DocumentNode, vars?: V, options?: C) => Promise<R> | AsyncIterable<R>
export function getSdk<C, E>(requester: Requester<C, E>) {
  return {
    LendingsQuery(variables?: LendingsQueryQueryVariables, options?: C): Promise<LendingsQueryQuery> {
      return requester<LendingsQueryQuery, LendingsQueryQueryVariables>(LendingsQueryDocument, variables, options) as Promise<LendingsQueryQuery>;
    }
  };
}
export type Sdk = ReturnType<typeof getSdk>;