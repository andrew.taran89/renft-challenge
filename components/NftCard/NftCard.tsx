/* eslint-disable @next/next/no-img-element */
import { FC, forwardRef, useRef } from "react";
import { Lending } from ".graphclient";
import { HeartIcon } from "components/Icons";
import hexToNumber from "utils/hexToNumber";
import type { Nft } from "alchemy-sdk";

export interface NftCardProps {
  lending: Partial<Lending>;
  metadata?: Nft;
  favorite?: boolean;
  toggleFavorite: (id: string) => void;
}

// eslint-disable-next-line react/display-name
const NftCard: FC<NftCardProps> = forwardRef<HTMLDivElement, NftCardProps>(
  (props, ref) => {
    const { lending, metadata, toggleFavorite, favorite } = props;
    const imgRef = useRef<HTMLImageElement>(null);
    const dailyRentPrice = hexToNumber(lending.dailyRentPrice);
    const collateral = hexToNumber(lending.nftPrice);
    const isAvailable = lending.renting === null;

    const handleImageError = () => {
      if (imgRef.current) {
        imgRef.current.src = "/image-1@2x.jpeg";
      }
    };

    return (
      <div
        ref={ref}
        className="flex relative flex-col p-2 bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700"
      >
        <button
          type="button"
          onClick={() => toggleFavorite(lending.id!)}
          className={`absolute top-4 right-4 bg-gray-800 hover:opacity-100 focus:outline-none rounded-lg p-1.5 flex items-center ${
            !favorite && "opacity-40"
          }`}
        >
          <HeartIcon className={`${favorite && "fill-[#E0144C]"}`} />
        </button>
        <img
          ref={imgRef}
          className="rounded-lg mb-3 bg-black object-cover h-[300px]"
          alt="Cover"
          onError={handleImageError}
          src={metadata?.media[0]?.gateway}
        />
        <h4
          title={metadata?.contract.name}
          className="mt-auto text-sm dark:text-white text-ellipsis overflow-hidden whitespace-nowrap"
        >
          {metadata?.contract.name}
        </h4>
        <h5
          title={metadata?.title}
          className="mt-auto text-lg dark:text-white text-ellipsis overflow-hidden whitespace-nowrap"
        >
          {metadata?.title}
        </h5>
        <div className="mt-2 text-sm dark:text-white flex">
          <h6>Collateral</h6>
          <h6 className="ml-2 font-semibold">{collateral}</h6>
        </div>
        <div className="mb-2 text-sm dark:text-white flex">
          <h6>Price</h6>
          <h6 className="ml-2 font-semibold">{dailyRentPrice} / day</h6>
        </div>
        {isAvailable && (
          <span className="mt-1 bg-green-50 text-green-600 text-sm text-center font-medium px-2.5 py-0.5 rounded opacity-80 dark:bg-green-200 dark:text-green-800">
            Available
          </span>
        )}
      </div>
    );
  }
);

export default NftCard;
