import { useRef, useState } from "react";
import useObserver from "hooks/useObserver";
import { getNftMetadata } from "services/alchemy";
import type { Nft } from "alchemy-sdk";
import type { NftCardProps } from "./NftCard";

// eslint-disable-next-line react/display-name
const withObserver = (WrappedComponent: any) => (props: NftCardProps) => {
  const {
    lending: { nftAddress, tokenId, isERC721 },
  } = props;
  const ref = useRef<HTMLDivElement>(null);
  const [metadata, setMetadata] = useState<Nft>();

  const fetchNFTMEtadata = async () => {
    if (metadata) return;

    const data = await getNftMetadata(nftAddress, tokenId, isERC721);

    setMetadata(data);
  };

  useObserver(ref, fetchNFTMEtadata);

  return <WrappedComponent {...props} metadata={metadata} ref={ref} />;
};

export default withObserver;
