import { fireEvent, getByText, render } from "@testing-library/react";
import { NftCard } from "components/NftCard";
import "@testing-library/jest-dom";
import hexToNumber from "utils/hexToNumber";
import { Nft } from "alchemy-sdk";

jest.mock("hooks/useObserver");

jest.mock("services/alchemy", () => ({
  getNftMetadata: jest.fn(),
}));

const toggleFavoriteMock = jest.fn();

describe("NftCard", () => {
  const nftMock = {
    id: "12312321412",
    nftAddress: "1312312312",
    tokenId: "1313123",
    nftPrice: "0x9184E729FFF",
    dailyRentPrice: "0x00000064",
    isERC721: true,
  };

  it("toggle favorite with id", () => {
    const { getByRole } = render(
      <NftCard
        lending={nftMock}
        toggleFavorite={toggleFavoriteMock}
        favorite={false}
      />
    );

    fireEvent.click(getByRole("button"));

    expect(toggleFavoriteMock).toHaveBeenCalledTimes(1);
    expect(toggleFavoriteMock).toHaveBeenCalledWith(nftMock.id);
  });

  it("display collateral", () => {
    const { getByText } = render(
      <NftCard
        lending={nftMock}
        toggleFavorite={toggleFavoriteMock}
        favorite={false}
      />
    );

    expect(getByText(hexToNumber(nftMock.nftPrice))).toBeTruthy();
  });

  it("display price per day", () => {
    const { getByText } = render(
      <NftCard
        lending={nftMock}
        toggleFavorite={toggleFavoriteMock}
        favorite={false}
      />
    );

    const dailyPriceText = `${hexToNumber(nftMock.dailyRentPrice)} / day`;

    expect(getByText(dailyPriceText)).toBeTruthy();
  });

  it("display title", () => {
    const matadataMock = {
      title: "Title",
      media: [],
      contract: {},
    };
    const { getByText } = render(
      <NftCard
        lending={nftMock}
        metadata={matadataMock as unknown as Nft}
        toggleFavorite={toggleFavoriteMock}
        favorite={false}
      />
    );

    expect(getByText(matadataMock.title)).toBeTruthy();
  });
});
