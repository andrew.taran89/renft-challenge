import NftCard from "./NftCard";
import withMetadata from "./withObserver";

const NftCardObserver = withMetadata(NftCard);

export { NftCard, NftCardObserver };
