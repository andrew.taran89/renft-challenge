import React, {
  createContext,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from "react";
import { storage } from "services/storage";

interface FavoritesContext {
  favorites: Record<string, boolean>;
  toggleFavorite: (id: string) => void;
}

const FavoritesContext = createContext<FavoritesContext>({
  favorites: {},
  toggleFavorite: () => {},
});

export const FavoritesContextProvider: React.FC<{
  children: React.ReactNode;
}> = (props) => {
  const { children } = props;
  const [favorites, setFavorites] = useState<Record<string, boolean>>({});

  const toggleFavorite = useCallback(
    (id: string) => {
      const state = { ...favorites, [id]: !favorites?.[id] };

      setFavorites(state);
      storage.saveRecord<Record<string, boolean>>("favorites", state);
    },
    [setFavorites, favorites]
  );

  const context = useMemo(
    () => ({ favorites, toggleFavorite }),
    [favorites, toggleFavorite]
  );

  useEffect(() => {
    const favoritesRecord =
      storage.getRecord<Record<string, boolean>>("favorites");

    setFavorites(favoritesRecord);
  }, []);

  return (
    <FavoritesContext.Provider value={context}>
      {children}
    </FavoritesContext.Provider>
  );
};

export function useFavoritesContext(): FavoritesContext {
  return useContext(FavoritesContext);
}
