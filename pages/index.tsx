import Head from "next/head";
import type { NextPage } from "next";
import Gallery from "screens/Gallery";

const Home: NextPage = () => (
  <>
    <Head>
      <title>Re-NFT challenge</title>
      <meta name="description" content="Re-NFT challenge" />
    </Head>
    <Gallery />
  </>
);

export default Home;
