import "styles/globals.css";
import type { AppProps } from "next/app";
import { FavoritesContextProvider } from "contexts/FavoritesContext";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <FavoritesContextProvider>
      <Component {...pageProps} />
    </FavoritesContextProvider>
  );
}

export default MyApp;
