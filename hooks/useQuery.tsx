import { useEffect, useMemo, useState } from "react";
import { execute } from ".graphclient";
import type { GraphQLOperation } from "@graphql-mesh/types";
import type { GraphQLError } from "graphql/error/GraphQLError";

const useQuery = <D, V>(document: GraphQLOperation<D, V>, variables: V) => {
  const [data, setData] = useState<D>();
  const [loading, setLoading] = useState(false);
  const [errors, setErrors] = useState<readonly GraphQLError[]>();

  const result = useMemo(
    () => ({ data, loading, errors }),
    [data, errors, loading]
  );

  useEffect(() => {
    setLoading(true);

    execute(document, variables)
      .then(({ data, errors }) => {
        setData(data);
        setErrors(errors);
      })
      .catch((error) => console.error(error))
      .finally(() => setLoading(false));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return result;
};

export default useQuery;
